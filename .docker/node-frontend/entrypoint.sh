mode=$NODE_ENV;
echo -e "\e[1;33;40mmode: $mode\e[0m";

cd /var/www/html/;
npm install;
if [[ $mode == 'production' ]]; then
  npm run build;
else
  npm start;
fi
