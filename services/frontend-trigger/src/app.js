const express    = require('express');
const bodyParser = require('body-parser');
const app        = express();
const port       = 3000;
const portIO     = 5000;

const server = require('http').createServer(app);
const io     = require('socket.io')(server, {
          cors: {
              origin: '*'
          }
      })
;

app.use(express.json({limit: '250mb'}));
app.use(express.urlencoded({limit: '250mb'}));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));

// parse application/json
app.use(bodyParser.json());

const clients = {};

app.post('/emit-event', async(req, res) => {

    for (const key in clients) {
        clients[key].emit('pong', req.body);
    }
    res.send();
})

app.listen(port, () => {
    console.log(`Internal http app listening on port ${port}`);
})


server.listen(portIO, () => {
    console.log(`Public socket app listening on port ${portIO}`);
});

io.sockets.on('connection', async(socket) => {
    console.log(`Public socket connection`);
    clients[socket.id] = socket;
    socket.on('eba', (args) => {
        console.log(`Public socket eba`, args);
        socket.emit('pong', args);
    });
    socket.on('disconnect', function() {
        delete clients[socket.id];
        console.log('Public socket disconnected');
    });
})