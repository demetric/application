<?php

use App\Components\Log;
use App\ProtectedController;
use Symfony\Component\HttpKernel\Exception\HttpException;

require_once('../vendor/autoload.php');

try {
    (new ProtectedController())->exec();
} catch (HttpException $e) {
    http_response_code($e->getStatusCode());
} catch (Throwable $t) {
    
    Log::get()->critical($t);
}