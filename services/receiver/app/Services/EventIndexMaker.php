<?php

namespace App\Services;

use App\Components\Log;
use App\Components\Serializer\Serializer;
use App\Components\Types\Json;
use App\Repositories\Dto\Event;
use App\Repositories\Dto\EventIndex;
use App\Repositories\Dto\EventIndexPath;
use App\Repositories\EventIndexPathCacheRepository;
use App\Repositories\EventIndexPathRepository;
use App\Repositories\EventIndexRepository;
use JsonException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Throwable;

class EventIndexMaker
{
    private PropertyAccessorInterface $propertyAccessor;
    
    public function __construct(
        private $pathCache = new EventIndexPathCacheRepository(),
        private $path = new EventIndexPathRepository(),
        private $index = new EventIndexRepository()
    ) {
        $this->propertyAccessor = PropertyAccess::createPropertyAccessorBuilder()
            ->enableExceptionOnInvalidIndex()
            ->getPropertyAccessor();
    }
    
    /**
     * @param Event $event
     * @return EventIndex[]
     */
    public function create(Event $event): array
    {
        $eventIndexList = [];
        $body = $event->getBody();
        try {
            $jsonBody = $body->decode();
        } catch (JsonException $e) {
            Log::get()->alert($e);
            $jsonBody = [];
        }
        $pathList = $this->getPathList();
        
        foreach ($pathList as $path) {
            try {
                if ($this->propertyAccessor->isReadable($jsonBody, $path->getPath())) {
                    $value = $this->propertyAccessor->getValue($jsonBody, $path->getPath());
                    if (!is_array($value) && !$value) {
                        continue;
                    }
                    $eventIndexList[] = $this->index->create(
                        EventIndex::make()
                            ->setEventId($event->getId())
                            ->setPathId($path->getId())
                            ->setBody(new Json(json_encode($value, JSON_THROW_ON_ERROR)))
                            ->setOrigin(is_array($value) ? json_encode($value, JSON_THROW_ON_ERROR) : $value)
                    );
                }
            } catch (Throwable $t) {
                Log::get()->alert($t);
            }
            
        }
        
        return $eventIndexList;
    }
    
    /**
     * @return EventIndexPath[]
     */
    private function getPathList(): array
    {
        $list = $this->pathCache->getList();
        if ($list === null) {
            $list = $this->path->getList();
            $this->pathCache->setList($list);
        }
        
        return $list;
    }
}