<?php

namespace App\Services\FrontendTrigger\Dto;

use App\Repositories\Dto\Event;
use App\Repositories\Dto\EventIndex;

class EventTrigger extends Event
{
    /**
     * @var EventIndex[]
     */
    public array $eventIndexList;
    
    /**
     * @return array
     */
    public function getEventIndexList(): array
    {
        return $this->eventIndexList;
    }
    
    /**
     * @param array $eventIndexList
     * @return EventTrigger
     */
    public function setEventIndexList(array $eventIndexList): EventTrigger
    {
        $this->eventIndexList = $eventIndexList;
        return $this;
    }
}