<?php

namespace App\Services\FrontendTrigger;

use App\Components\Serializer\Serializer;
use app\Services\FrontendTrigger\Dto\EventTrigger;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class FrontendTriggerService
{
    private Client $client;
    
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => env('FRONTEND_TRIGGER_SERVICE_URL'),
            'timeout' => 15,
            'allow_redirects' => false,
            'headers' => ['Content-Type' => 'application/json'],
        ]);
    }
    
    /**
     * @throws GuzzleException
     */
    public function emitEvent(EventTrigger $event): void
    {
        $this->client->request('POST', '/emit-event', ['body' => Serializer::make()->serialize($event)]);
    }
}