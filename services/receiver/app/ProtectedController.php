<?php

namespace App;

use App\Components\Cache;
use App\Components\Request;
use App\Repositories\EventIndexPathCacheRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\VarDumper\VarDumper;

class ProtectedController
{
    private string $path;
    
    public function __construct()
    {
        $this->path = Request::get()->path();
    }
    
    /**
     * @return void
     */
    public function exec(): void
    {
        /** @noinspection DegradedSwitchInspection */
        switch ($this->path) {
            case 'cache/flushall':
                Cache::get()->flushall();
                break;
            default:
                throw new NotFoundHttpException();
        }
    }
}