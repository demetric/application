<?php

namespace App\Repositories;

use App\Components\BaseException;
use App\Components\Dto;
use App\Components\Db;
use App\Repositories\Dto\EventFail;
use App\Repositories\Dto\NewEventFail;
use App\Repositories\Exceptions\NotFoundException;
use Carbon\Carbon;

class EventFailRepository
{
    /**
     * @param NewEventFail $newEvent
     * @return EventFail
     * @throws BaseException
     */
    public function create(NewEventFail $newEvent): EventFail
    {
        $event = Dto::fill($newEvent, EventFail::make())
            ->setRead(false)
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());
        
        $id = Db::get()::table('event_fail')->insertGetId([
            'app_id' => $newEvent->getAppId(),
            'app_dsn' => $event->getAppDsn(),
            'body' => $newEvent->getBody(),
            'error_message' => $newEvent->getErrorMessage(),
            'read' => $event->isRead(),
            'created_at' => $event->getCreatedAt(),
            'updated_at' => $event->getUpdatedAt(),
        ]);
        
        $event->setId($id);
        
        if (!$id) {
            NotFoundException::throw();
        }
        
        return $event;
    }
    
}