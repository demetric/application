<?php

namespace App\Repositories\Exceptions;

use App\Components\BaseException;

class NotFoundException extends BaseException
{

}