<?php

namespace App\Repositories;

use App\Components\Db;
use App\Repositories\Exceptions\NotFoundException;
use Illuminate\Database\Query\JoinClause;

class ApplicationRepository
{
    /**
     * @throws NotFoundException
     */
    public function getIdByDsn(string $dsn): int
    {
        $id = Db::get()::table('application')
            ->addSelect('application.id')
            ->where('application.dsn', '=', $dsn)
            ->where('application.active', '=', true)
            ->join('project', function (JoinClause $join) {
                $join->on('application.project_id', '=', 'project.id')
                    ->where('project.active', '=', true);
            })
            ->pluck('id')
            ->first();
        if (!$id) {
            NotFoundException::throw();
        }
        return $id;
    }
    
}