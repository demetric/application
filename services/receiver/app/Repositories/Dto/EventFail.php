<?php

namespace App\Repositories\Dto;

use Carbon\Carbon;

class EventFail extends NewEventFail
{
    private int    $id;
    private bool   $read;
    private Carbon $createdAt;
    private Carbon $updatedAt;
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    /**
     * @param int $id
     * @return EventFail
     */
    public function setId(int $id): EventFail
    {
        $this->id = $id;
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isRead(): bool
    {
        return $this->read;
    }
    
    /**
     * @param bool $read
     * @return EventFail
     */
    public function setRead(bool $read): EventFail
    {
        $this->read = $read;
        return $this;
    }
    
    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon
    {
        return $this->createdAt;
    }
    
    /**
     * @param Carbon $createdAt
     * @return EventFail
     */
    public function setCreatedAt(Carbon $createdAt): EventFail
    {
        $this->createdAt = $createdAt;
        return $this;
    }
    
    /**
     * @return Carbon
     */
    public function getUpdatedAt(): Carbon
    {
        return $this->updatedAt;
    }
    
    /**
     * @param Carbon $updatedAt
     * @return EventFail
     */
    public function setUpdatedAt(Carbon $updatedAt): EventFail
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}