<?php

namespace App\Repositories\Dto;

use App\Components\BaseDto;

class EventIndexPath extends BaseDto
{
    private int $id;
    private string $path;
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    /**
     * @param int $id
     * @return EventIndexPath
     */
    public function setId(int $id): EventIndexPath
    {
        $this->id = $id;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }
    
    /**
     * @param string $path
     * @return EventIndexPath
     */
    public function setPath(string $path): EventIndexPath
    {
        $this->path = $path;
        return $this;
    }
}