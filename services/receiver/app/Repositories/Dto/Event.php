<?php

namespace App\Repositories\Dto;

use Carbon\Carbon;

class Event extends NewEvent
{
    private int    $id;
    private bool   $read;
    private Carbon $createdAt;
    private Carbon $updatedAt;
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    /**
     * @param int $id
     * $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isRead(): bool
    {
        return $this->read;
    }
    
    /**
     * @param bool $read
     * $this
     */
    public function setRead(bool $read): self
    {
        $this->read = $read;
        return $this;
    }
    
    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon
    {
        return $this->createdAt;
    }
    
    /**
     * @param Carbon $createdAt
     * $this
     */
    public function setCreatedAt(Carbon $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }
    
    /**
     * @return Carbon
     */
    public function getUpdatedAt(): Carbon
    {
        return $this->updatedAt;
    }
    
    /**
     * @param Carbon $updatedAt
     * $this
     */
    public function setUpdatedAt(Carbon $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}