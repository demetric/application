<?php

namespace App\Repositories\Dto;

use App\Components\BaseDto;
use App\Components\Types\Json;

class NewEvent extends BaseDto
{
    private int  $appId;
    private Json $body;
    
    /**
     * @return int
     */
    public function getAppId(): int
    {
        return $this->appId;
    }
    
    /**
     * @param int $appId
     * @return $this
     */
    public function setAppId(int $appId): self
    {
        $this->appId = $appId;
        return $this;
    }
    
    /**
     * @return Json
     */
    public function getBody(): Json
    {
        return $this->body;
    }
    
    /**
     * @param Json $body
     * @return NewEvent
     */
    public function setBody(Json $body): NewEvent
    {
        $this->body = $body;
        return $this;
    }
}