<?php

namespace App\Repositories\Dto;

use App\Components\BaseDto;

class NewEventFail extends BaseDto
{
    private ?int    $appId;
    private ?string $appDsn;
    private ?string $body;
    private ?string $errorMessage;
    
    /**
     * @return int|null
     */
    public function getAppId(): ?int
    {
        return $this->appId;
    }
    
    /**
     * @param int|null $appId
     * @return NewEventFail
     */
    public function setAppId(?int $appId): NewEventFail
    {
        $this->appId = $appId;
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getAppDsn(): ?string
    {
        return $this->appDsn;
    }
    
    /**
     * @param string|null $appDsn
     * @return NewEventFail
     */
    public function setAppDsn(?string $appDsn): NewEventFail
    {
        $this->appDsn = $appDsn;
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getBody(): ?string
    {
        return $this->body;
    }
    
    /**
     * @param string|null $body
     * @return NewEventFail
     */
    public function setBody(?string $body): NewEventFail
    {
        $this->body = $body;
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }
    
    /**
     * @param string|null $errorMessage
     * @return NewEventFail
     */
    public function setErrorMessage(?string $errorMessage): NewEventFail
    {
        $this->errorMessage = $errorMessage;
        return $this;
    }
    
}