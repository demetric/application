<?php

namespace App\Repositories\Dto;

use App\Components\BaseDto;
use App\Components\Types\Json;

class EventIndex extends BaseDto
{
    private int    $eventId;
    private int    $pathId;
    private Json $body;
    private string $origin;
    
    /**
     * @return int
     */
    public function getEventId(): int
    {
        return $this->eventId;
    }
    
    /**
     * @param int $eventId
     * @return EventIndex
     */
    public function setEventId(int $eventId): EventIndex
    {
        $this->eventId = $eventId;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getPathId(): int
    {
        return $this->pathId;
    }
    
    /**
     * @param int $pathId
     * @return EventIndex
     */
    public function setPathId(int $pathId): EventIndex
    {
        $this->pathId = $pathId;
        return $this;
    }
    
    /**
     * @return Json
     */
    public function getBody(): Json
    {
        return $this->body;
    }
    
    /**
     * @param Json $body
     * @return EventIndex
     */
    public function setBody(Json $body): EventIndex
    {
        $this->body = $body;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getOrigin(): string
    {
        return $this->origin;
    }
    
    /**
     * @param string $origin
     * @return EventIndex
     */
    public function setOrigin(string $origin): EventIndex
    {
        $this->origin = $origin;
        return $this;
    }
}