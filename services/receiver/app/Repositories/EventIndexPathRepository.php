<?php

namespace App\Repositories;

use App\Components\Db;
use App\Components\Dto;
use App\Components\Log;
use App\Repositories\Dto\EventIndexPath;

class EventIndexPathRepository
{
    /**
     * @return EventIndexPath[]
     */
    public function getList(): array
    {
       
        $list = Db::get()::table('event_index_path')->select(['id', 'path'])->get();
        return Dto::makeFromDataList($list, EventIndexPath::class);
    }
}