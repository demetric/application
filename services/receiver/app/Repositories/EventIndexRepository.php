<?php

namespace App\Repositories;

use App\Components\Db;
use App\Repositories\Dto\EventIndex;

class EventIndexRepository
{
    public function create(EventIndex $eventIndex): EventIndex
    {
        Db::get()::table('event_index')->insert([
            'event_id' => $eventIndex->getEventId(),
            'path_id' => $eventIndex->getPathId(),
            'body' => $eventIndex->getBody()->toString(),
            'origin' => $eventIndex->getOrigin(),
        ]);
        
        return $eventIndex;
    }
}