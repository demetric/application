<?php

namespace App\Repositories;

use App\Components\Cache;
use App\Components\Serializer\Serializer;
use App\Repositories\Dto\EventIndexPath;

class EventIndexPathCacheRepository
{
    private const CACHE_KEY = 'table_event_index_path';
    
    /**
     * @param EventIndexPath[] $eventIndexPathList
     * @return void
     */
    public function setList(array $eventIndexPathList): void
    {
        Cache::get()->set(self::CACHE_KEY, Serializer::make()->serialize($eventIndexPathList));
    }
    
    /**
     * @return EventIndexPath[]|null
     */
    public function getList(): ?array
    {
        $cache = Cache::get()->get(self::CACHE_KEY);
        if (!$cache) {
            return null;
        }
        
        return Serializer::make()->deserializeList($cache, EventIndexPath::class . '[]');
    }
}