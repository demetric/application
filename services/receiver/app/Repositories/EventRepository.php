<?php

namespace App\Repositories;

use App\Components\Dto;
use App\Components\Db;
use App\Repositories\Dto\Event;
use App\Repositories\Dto\NewEvent;
use App\Repositories\Exceptions\NotFoundException;
use Carbon\Carbon;

class EventRepository
{
    /**
     * @param NewEvent $newEvent
     * @return Event
     * @throws NotFoundException
     */
    public function create(NewEvent $newEvent): Event
    {
        $event = Dto::fill($newEvent, Event::make())
            ->setRead(false)
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());
        
        $id = Db::get()::table('event')->insertGetId([
            'app_id' => $newEvent->getAppId(),
            'body' => $newEvent->getBody()->toString(),
            'origin' => $newEvent->getBody()->toString(),
            'read' => $event->isRead(),
            'created_at' => $event->getCreatedAt(),
            'updated_at' => $event->getUpdatedAt(),
        ]);

        $event->setId($id);
        
        if (!$id) {
            NotFoundException::throw();
        }
        
        return $event;
    }
    
}