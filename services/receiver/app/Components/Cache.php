<?php

namespace App\Components;

use Predis\Client;

class Cache
{
    private static ?Client $redis = null;
    
    public static function get(): Client
    {
        if (self::$redis === null) {
            self::$redis = new Client([
                'scheme' => 'tcp',
                'url' => env('REDIS_URL'),
                'host' => env('REDIS_HOST', '127.0.0.1'),
                'username' => env('REDIS_USERNAME'),
                'password' => env('REDIS_PASSWORD'),
                'port' => env('REDIS_PORT', '6379'),
                'database' => env('REDIS_CACHE_DB', '1'),
            ], [
                'prefix' => 'receiver_',
            ]);
        }
        
        return self::$redis;
    }
}