<?php

namespace App\Components;

use JsonException;

class Request
{
    private static self|null $instance = null;
    
    private string|false $body;
    
    private string $path;
    
    /**
     * @var array|null
     */
    private mixed $json;
    private array $headers;
    
    /**
     * @throws JsonException
     */
    protected function __construct()
    {
        $this->path = (string)parse_url(preg_replace('/^\//', '', $_SERVER['REQUEST_URI']))['path'];
        $this->body = file_get_contents('php://input');
        $this->json = $this->body ? json_decode($this->body, true, 512, JSON_THROW_ON_ERROR) : null;
        $this->headers = getallheaders() ?: [];
    }
    
    public static function get(): self
    {
        if (!self::$instance) {
            self::$instance = new static();
        }
        return self::$instance;
    }
    
    public function path(): string
    {
        return $this->path;
    }
    
    public function body(): string
    {
        return (string)$this->body;
    }
    
    public function json(): ?array
    {
        return $this->json;
    }
    
    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }
    
    public function hasHeader(string $header): bool
    {
        return array_key_exists($header, $this->headers);
    }
    
    public function getHeader(string $header): ?string
    {
        return $this->headers[$header] ?? null;
    }
    
    public function __clone()
    {
    
    }
    
    public function __wakeup()
    {
    
    }
}
