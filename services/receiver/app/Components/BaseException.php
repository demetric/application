<?php

namespace App\Components;

use Exception;
use Throwable;

abstract class BaseException extends Exception
{
    /**
     * @throws $this
     */
    public static function throw(string $message = '', int $code = 0, ?Throwable $previous = null): void
    {
        throw new static($message, $code, $previous);
    }
}