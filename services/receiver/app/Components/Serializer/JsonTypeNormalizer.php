<?php

namespace App\Components\Serializer;

use App\Components\Types\Json;
use ArrayObject;
use JsonException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class JsonTypeNormalizer implements NormalizerInterface
{
    /**
     * @inheritDoc
     * @throws JsonException
     */
    public function normalize(mixed $object, string $format = null, array $context = []): float|int|bool|ArrayObject|array|string|null
    {
        /**
         * @var $object Json
         */
       
        return $object->decode();
    }
    
    /**
     * @inheritDoc
     */
    public function supportsNormalization(mixed $data, string $format = null): bool
    {
        return $data instanceof Json;
    }
}