<?php

namespace App\Components\Serializer;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer as SymfonySerializer;

class Serializer
{
    private SymfonySerializer $serializer;
    
    public const F_JSON = 'json';
    
    public function __construct(ObjectNormalizer $objectNormalizer = new ObjectNormalizer(), JsonEncoder $jsonEncoder = new JsonEncoder())
    {
        $this->serializer = new SymfonySerializer(
            [
                new CarbonNormalizer(),
                new JsonTypeNormalizer(),
                $objectNormalizer,
            ],
            ['json' => $jsonEncoder]
        );
    }
    
    public static function make(): self
    {
        return new static();
    }
    
    public function serialize($data, string $format = self::F_JSON): string
    {
        return $this->serializer->serialize($data, $format);
    }
    
    public function deserialize($data, string $className, string $format = self::F_JSON): mixed
    {
        return $this->serializer->deserialize($data, $className, $format);
    }
    
    public function deserializeList($data, string $className, string $format = self::F_JSON): mixed
    {
        $serializer = new SymfonySerializer(
            [new GetSetMethodNormalizer(), new ArrayDenormalizer()],
            [new JsonEncoder()]
        );
        return $serializer->deserialize($data, $className, $format);
    }
}
