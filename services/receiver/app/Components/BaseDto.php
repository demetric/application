<?php

namespace App\Components;

abstract class BaseDto
{
    public static function make(): static
    {
        return new static();
    }
}