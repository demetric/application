<?php /** @noinspection PhpMissingFieldTypeInspection */

namespace App\Components;

use Illuminate\Database\Capsule\Manager as Capsule;

class Db
{
    private static ?Capsule $db = null;
    
    public static function get(): Capsule
    {
        if (self::$db === null) {
            self::$db = new Capsule();
            self::$db->addConnection([
                'driver' => 'mysql',
                'host' => env('DB_HOST'),
                'database' => env('DB_DATABASE'),
                'username' => env('DB_USERNAME'),
                'password' => env('DB_PASSWORD'),
            ]);
            self::$db->setAsGlobal();
            self::$db->bootEloquent();
        }
        
        return self::$db;
    }
}
