<?php

namespace App\Components;

use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

class Log
{
    public static function get(string $name = 'app'): Logger
    {
        $logger = new Logger($name);
        $logger->pushHandler(new RotatingFileHandler('/var/www/html/runtime/logs/app.log'));
        
        return $logger;
    }
}