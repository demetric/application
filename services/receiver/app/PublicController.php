<?php

namespace App;

use App\Components\Dto;
use App\Components\Log;
use App\Components\Request;
use App\Components\Types\Json;
use App\Repositories\ApplicationRepository;
use App\Repositories\Dto\NewEvent;
use App\Repositories\Dto\NewEventFail;
use App\Repositories\EventFailRepository;
use App\Repositories\EventRepository;
use App\Services\EventIndexMaker;
use App\Services\FrontendTrigger\Dto\EventTrigger;
use App\Services\FrontendTrigger\FrontendTriggerService;
use Exception;
use Throwable;

class PublicController
{
    private string $dsn;
    private EventFailRepository $eventFailRepository;
    private ApplicationRepository $applicationRepository;
    private EventRepository $eventRepository;
    private EventIndexMaker $eventIndexMaker;
    private FrontendTriggerService $frontendTrigger;

    public function __construct()
    {
        $this->dsn = Request::get()->path();

        $this->applicationRepository = new ApplicationRepository();
        $this->eventRepository = new EventRepository();
        $this->eventFailRepository = new EventFailRepository();
        $this->eventIndexMaker = new EventIndexMaker();
        $this->frontendTrigger = new FrontendTriggerService();
    }

    /**
     * @return void
     */
    public function exec(): void
    {
        $appId = null;
        try {
            //TODO Cache repo
            $appId = $this->applicationRepository->getIdByDsn($this->dsn);
        } catch (Throwable $t) {
            Log::get()->alert($t);
        }
        try {
            $body = Request::get()->json();
            $isPull = Request::get()->getHeader('Body-Type') === 'pull';
            $pull = $isPull ? $body : [$body];

            if (!$body) {
                /** @noinspection ThrowRawExceptionInspection */
                throw new Exception('body must not be empty');
            }
            if (!$appId) {
                /** @noinspection ThrowRawExceptionInspection */
                throw new Exception('appId must not be empty');
            }

            foreach ($pull as $value) {
                try {
                    $this->saveEvent($appId, $value);
                } catch (Throwable $t) {
                    $this->fail($t, $appId, $value);
                }
            }
        } catch (Throwable $t) {
            $this->fail($t, $appId);
        }
    }

    /**
     * @param $appId
     * @param $body
     * @return void
     * @throws Throwable
     */
    private function saveEvent($appId, $body): void
    {
        $newEvent = NewEvent::make()
            ->setBody(new Json(json_encode($body, JSON_THROW_ON_ERROR)))
            ->setAppId($appId);
        $event = $this->eventRepository->create($newEvent);
        $eventIndexList = [];
        try {
            $eventIndexList = $this->eventIndexMaker->create($event);
        } catch (Throwable $t) {
            Log::get()->error($t);
        }
        $eventTrigger = Dto::fill($event, EventTrigger::make())->setEventIndexList($eventIndexList);
        $this->frontendTrigger->emitEvent($eventTrigger);
    }

    private function fail($t, $appId, $body = null): void
    {
        Log::get()->critical($t);
        try {
            $newEvent = NewEventFail::make()
                ->setBody($body ?: Request::get()->body())
                ->setAppDsn($this->dsn)
                ->setAppId($appId)
                ->setErrorMessage($t->getMessage());
            $this->eventFailRepository->create($newEvent);
        } catch (Throwable $t) {
            Log::get()->critical($t);
        }
    }
}
