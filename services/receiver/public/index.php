<?php

use App\Components\Log;
use App\PublicController;

require_once('../vendor/autoload.php');

try {
    (new PublicController())->exec();
} catch (Throwable $t) {
    Log::get()->critical($t);
}