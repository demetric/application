<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('event_index', static function (Blueprint $table) {
            $table->integer('event_id')->unsigned();
            $table->integer('path_id')->unsigned();
            $table->primary(['event_id', 'path_id']);
            $table->json('body');
            $table->longText('origin')->nullable();
        });
    
        Schema::table('event_index', static function (Blueprint $table) {
            $table->foreign('event_id')->references('id')->on('event')->onDelete('cascade');
            $table->foreign('path_id')->references('id')->on('event_index_path')->onDelete('cascade');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('event_index', static function (Blueprint $table) {
            $table->dropForeign('event_index_path_id_foreign');
            $table->dropForeign('event_id_foreign');
        });
        Schema::dropIfExists('event_index');
    }
};
