<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_fail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('app_dsn')->nullable();
            $table->integer('app_id')->unsigned()->nullable();
            $table->boolean('read')->default(0);
            $table->longText('body')->nullable();
            $table->string('error_message')->nullable();
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_fail');
    }
};
