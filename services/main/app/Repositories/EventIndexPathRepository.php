<?php

namespace App\Repositories;

use App\Components\Builders\OffsetLimitQueryBuilder;
use App\Components\Builders\OrderByQueryBuilder;
use App\Components\Dto;
use App\Components\Repository\Contracts\ListChunkRepositoryInterface;
use App\Components\Repository\Exceptions\NotFoundException;
use App\Dto\EventIndexPath;
use App\Dto\NewEventIndexPath;
use App\Services\ReceiverProtected\ReceiverProtectedManager;
use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Throwable;

class EventIndexPathRepository implements ListChunkRepositoryInterface
{
    public function __construct(
        private readonly OrderByQueryBuilder $orderByBuilder,
        private readonly OffsetLimitQueryBuilder $offsetLimitBuilder,
        private readonly ReceiverProtectedManager $receiverProtectedManager
    ) {
    }
    
    /**
     * @return EventIndexPath[]
     */
    public function getList(): array
    {
        $listQuery = DB::table('event_index_path');
        /** @noinspection PhpExpressionResultUnusedInspection */
        $this->fillBuilder($listQuery);
        $this->orderByBuilder->snakeCase()->filter(['id', 'path', 'altName', 'createdAt', 'updatedAt', 'isHidden', 'sort'])->set($listQuery);
        $this->offsetLimitBuilder->set($listQuery);
        return Dto::makeFromDataList($listQuery->get(), EventIndexPath::class);
    }
    
    public function getCount(): int
    {
        $countQuery = DB::table('event_index_path');
        /** @noinspection PhpExpressionResultUnusedInspection */
        $this->fillBuilder($countQuery);
        return $countQuery->count();
    }
    
    /**
     * @param NewEventIndexPath $newEventIndexPath
     * @return EventIndexPath
     * @throws NotFoundException
     * @throws Throwable
     */
    public function create(NewEventIndexPath $newEventIndexPath): EventIndexPath
    {
        $eventIndexPath = Dto::fill($newEventIndexPath, EventIndexPath::make())
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());
        $id = Db::table('event_index_path')->insertGetId([
            ...Dto::toSnakeArray($newEventIndexPath),
            'created_at' => $eventIndexPath->getCreatedAt(),
            'updated_at' => $eventIndexPath->getUpdatedAt(),
        ]);
        NotFoundException::checkAndThrow($id);
        $eventIndexPath->setId($id);
        $this->receiverProtectedManager->cache()->flushall();
        return $eventIndexPath;
    }
    
    /**
     * @param int $id
     * @return EventIndexPath
     * @throws NotFoundException
     */
    public function find(int $id): EventIndexPath
    {
        $item = DB::table('event_index_path')->find($id);
        NotFoundException::checkAndThrow($item);
        return Dto::makeFromDataItem($item, EventIndexPath::class);
    }
    
    /**
     * @param int               $id
     * @param NewEventIndexPath $newEventIndexPath
     * @return void
     * @throws BindingResolutionException
     * @throws GuzzleException
     * @throws NotFoundException
     * @throws ExceptionInterface
     */
    public function update(int $id, NewEventIndexPath $newEventIndexPath): void
    {
        $hasId = Db::table('event_index_path')
            ->where('id', $id)
            ->update([
                ...Dto::toSnakeArray($newEventIndexPath),
                'updated_at' => Carbon::now(),
            ]);
        NotFoundException::checkAndThrow($hasId);
        $this->receiverProtectedManager->cache()->flushall();
    }
    
    /**
     * @param int $id
     * @throws NotFoundException
     * @throws Throwable
     */
    public function delete(int $id): void
    {
        $id = Db::table('event_index_path')->delete($id);
        $this->receiverProtectedManager->cache()->flushall();
        NotFoundException::checkAndThrow($id);
    }
    
    /** @noinspection PhpReturnValueOfMethodIsNeverUsedInspection */
    private function fillBuilder(Builder $builder): Builder
    {
        return $builder;
    }
}