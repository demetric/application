<?php

namespace App\Repositories;

use App\Components\Builders\OffsetLimitQueryBuilder;
use App\Components\Builders\OrderByQueryBuilder;
use App\Components\Dto;
use App\Components\Repository\Contracts\ListChunkRepositoryInterface;
use App\Dto\Event;
use App\Dto\EventIndex;
use App\Http\Requests\EventListRequest;
use App\Services\EventFilter\EventFilterQueryBuilder;
use App\Services\EventFilter\Exceptions\UndefinedConditionException;
use App\Services\EventFilter\Exceptions\UndefinedFunctionException;
use App\Services\EventFilter\Exceptions\UndefinedGroupConditionException;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

class EventRepository implements ListChunkRepositoryInterface
{
    public function __construct(
        private readonly EventListRequest $eventListRequest,
        private readonly OrderByQueryBuilder $orderByBuilder,
        private readonly OffsetLimitQueryBuilder $offsetLimitBuilder,
        private readonly EventFilterQueryBuilder $filterQueryBuilder,
    ) {
    }
    
    /**
     * @return Event[]
     * @throws UndefinedConditionException
     * @throws UndefinedFunctionException
     * @throws UndefinedGroupConditionException
     */
    public function getList(): array
    {
        $listQuery = DB::table('event')->select(['event.*']);
        $this->fillBuilder($listQuery);
        $this->orderByBuilder->snakeCase()->filter(['id', 'name', 'createdAt', 'updatedAt'])->set($listQuery);
        $this->offsetLimitBuilder->set($listQuery);
        $eventList = $listQuery->get();
        $ids = $eventList->pluck('id')->toArray();
        if ($ids) {
            $eventIndexList = DB::table('event_index')->whereIn('event_id', $ids)->get();
            foreach ($eventIndexList as $item){
                if (!is_numeric($item->body)) {
                    $item->body = $item->origin;
                }
            }
            $eventIndexList = $eventIndexList->groupBy(['event_id']);
            foreach ($eventList as $eventItem) {
                if ($eventIndexList->has($eventItem->id)) {
                    $eventItem->event_index_list = Dto::makeFromDataList($eventIndexList->get($eventItem->id), EventIndex::class);
                } else {
                    $eventItem->event_index_list = [];
                }
            }
        }
        
        return Dto::makeFromDataList($eventList, Event::class);
    }
    
    /**
     * @throws UndefinedFunctionException
     * @throws UndefinedGroupConditionException
     * @throws UndefinedConditionException
     */
    public function getCount(): int
    {
        $countQuery = DB::table('event');
        $this->fillBuilder($countQuery);
        return $countQuery->count();
    }
    
    /** @noinspection PhpReturnValueOfMethodIsNeverUsedInspection */
    /**
     * @throws UndefinedFunctionException
     * @throws UndefinedGroupConditionException
     * @throws UndefinedConditionException
     */
    private function fillBuilder(Builder $builder): Builder
    {
        if ($this->eventListRequest->getAppId()) {
            $builder->whereIn('app_id', $this->eventListRequest->getAppId());
        }
        $this->filterQueryBuilder->buildQuery($builder);
        return $builder;
    }
    
    public function truncate(): void
    {
        DB::table('event_index')->truncate();
        DB::select('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('event')->truncate();
        DB::select('SET FOREIGN_KEY_CHECKS = 1');
    }
}
