<?php

namespace App\Components\Types;

use JsonException;

class Json
{
    public function __construct(private readonly string $stringData)
    {
    
    }
    
    /**
     * @throws JsonException
     */
    public function decode()
    {
        return json_decode($this->stringData, true, 512, JSON_THROW_ON_ERROR);
    }
    
    public function toString(): string
    {
        return $this->stringData;
    }
}