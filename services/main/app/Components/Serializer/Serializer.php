<?php

namespace App\Components\Serializer;

use App\Components\Builders\PageResultDtoBuilder;
use App\Components\Repository\Contracts\ListChunkRepositoryInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer as SymfonySerializer;

class Serializer
{
    private SymfonySerializer $serializer;
    
    public const F_JSON = 'json';
    
    public function __construct(ObjectNormalizer $objectNormalizer, JsonEncoder $jsonEncoder)
    {
        $this->serializer = new SymfonySerializer(
            [
                resolve(CarbonNormalizer::class),
                resolve(JsonTypeNormalizer::class),
                $objectNormalizer,
            ],
            ['json' => $jsonEncoder]
        );
    }
    
    public function serialize($data, string $format = self::F_JSON): string
    {
        return $this->serializer->serialize($data, $format);
    }
    
    public function deserialize($data, string $className, string $format = self::F_JSON): mixed
    {
        return $this->serializer->deserialize($data, $className, $format);
    }
    
    public function deserializeList($data, string $className, string $format = self::F_JSON): mixed
    {
        $serializer = new SymfonySerializer(
            [new GetSetMethodNormalizer(), new ArrayDenormalizer()],
            [new JsonEncoder()]
        );
        return $serializer->deserialize($data, $className, $format);
    }
    
    public function response(mixed $data, string $format = self::F_JSON): Response
    {
        return $this->jsonContent($this->serialize($data, $format));
    }
    
    public function responseOneField($value, string $field = 'id'): Response
    {
        return $this->response([$field => $value]);
    }
    
    public function listChunkResponse(ListChunkRepositoryInterface $listChunkRepository): Response
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $pageResultDtoMaker = app()->make(PageResultDtoBuilder::class);
        /* @var $pageResultDtoMaker PageResultDtoBuilder */
        return $this->response(
            $pageResultDtoMaker
                ->setList($listChunkRepository->getList())
                ->setCount($listChunkRepository->getCount())
                ->make()
        );
    }
    
    private function jsonContent(string $content): Response
    {
        $response = new Response();
        $response->setContent($content);
        $response->headers->set('Content-Type', 'application/json');
        
        return $response;
    }
}
