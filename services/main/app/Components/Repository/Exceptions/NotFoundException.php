<?php

namespace App\Components\Repository\Exceptions;

use App\Components\BaseException;

class NotFoundException extends BaseException
{
    /**
     * @param $data
     * @return void
     * @throws NotFoundException
     */
    public static function checkAndThrow($data): void
    {
        if (!$data) {
            throw new self();
        }
    }
}