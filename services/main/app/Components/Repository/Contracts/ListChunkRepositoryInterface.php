<?php

namespace App\Components\Repository\Contracts;

use App\Components\BaseDto;

interface ListChunkRepositoryInterface
{
    /**
     * @return BaseDto[]
     */
    public function getList(): array;
    
    /**
     * @return int
     */
    public function getCount(): int;
}