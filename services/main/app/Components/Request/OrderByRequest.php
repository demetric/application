<?php

namespace App\Components\Request;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class OrderByRequest
{
    public const DEFAULT_ORDER_BY = 'id';
    private string $orderDirection;
    private string $orderBy;
    
    public function __construct(Request $request)
    {
        $all = $request->all();
        $this->orderBy = (string)Arr::get($all, 'orderBy', self::DEFAULT_ORDER_BY);
        $this->orderDirection = (string)Arr::get($all, 'orderDirection', 'desc');
        
        if (!in_array($this->orderDirection, [
            'asc',
            'desc',
        ], true)) {
            $this->orderDirection = 'asc';
        }
    }
    
    /**
     * @return string
     */
    public function getOrderDirection(): string
    {
        return $this->orderDirection;
    }
    
    /**
     * @return string
     */
    public function getOrderBy(): string
    {
        return $this->orderBy;
    }
}