<?php

namespace App\Components\Request;

use App\Components\Dto\PageDataInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class PageRequest implements PageDataInterface
{
    private const DEFAULT_LIMIT = 25;
    private int $page;
    private int $perPage;
    
    public function __construct(Request $request)
    {
        $all = $request->all();
        $this->page = (int)Arr::get($all, 'page', 1) ?: 1;
        $this->perPage = (int)Arr::get($all, 'perPage', self::DEFAULT_LIMIT);
    }
    
    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }
    
    /**
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }
}