<?php

namespace App\Components\Request;

class PaginationRequest
{
    private int $offset;
    private int $limit;
    
    public function __construct(PageRequest $pageRequest)
    {
        $this->offset = $pageRequest->getPerPage() * ($pageRequest->getPage() - 1);
        $this->limit = $pageRequest->getPerPage();
    }
    
    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }
    
    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }
}