<?php

namespace App\Components;

use ReflectionMethod;

abstract class DtoReflectionHelper
{
    public static function getter(ReflectionMethod $method): ?string
    {
        if (!$method->isPublic() || $method->isStatic() || count($method->getParameters()) !== 0) {
            return null;
        }
        return preg_replace('/^(get|is)([A-Z].+$)/', '$2', $method->name);
    }
    
    public static function setter(ReflectionMethod $method): ?string
    {
        if (!$method->isPublic() || $method->isStatic() || count($method->getParameters()) === 0) {
            return null;
        }
        return preg_replace('/^(set|is|setIs)([A-Z].+$)/', '$2', $method->name);
    }
}