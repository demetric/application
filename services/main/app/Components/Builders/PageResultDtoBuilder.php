<?php

namespace App\Components\Builders;

use App\Components\Dto\PageResultDto;
use App\Components\Dto\PaginationResultDto;
use App\Components\Request\PageRequest;

class PageResultDtoBuilder
{
    private array $list  = [];
    private int   $count = 0;
    
    public function __construct(private readonly PageRequest $request)
    {
    }
    
    /**
     * @param array $list
     * @return PageResultDtoBuilder
     */
    public function setList(array $list): self
    {
        $this->list = $list;
        return $this;
    }
    
    /**
     * @param int $count
     * @return PageResultDtoBuilder
     */
    public function setCount(int $count): self
    {
        $this->count = $count;
        return $this;
    }
    
    public function make(): PageResultDto
    {
        return PageResultDto::make()
            ->setPagination(
                PaginationResultDto::make()
                    ->setPage($this->request->getPage())
                    ->setPerPage($this->request->getPerPage())
                    ->setCount($this->count))
            ->setList($this->list);
    }
}