<?php

namespace App\Components\Builders;

use App\Components\Request\PaginationRequest;
use Illuminate\Database\Query\Builder;

class OffsetLimitQueryBuilder
{
    public function __construct(private readonly PaginationRequest $paginationRequest)
    {
    }
    
    public function set(Builder $builder): Builder
    {
        return $builder
            ->offset($this->paginationRequest->getOffset())
            ->limit($this->paginationRequest->getLimit());
    }
}