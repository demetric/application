<?php

namespace App\Components\Builders;

use App\Components\Request\OrderByRequest;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Str;

class OrderByQueryBuilder
{
    private bool        $useSnake       = false;
    private array|null  $filterFields   = null;
    private string|null $defaultOrderBy = OrderByRequest::DEFAULT_ORDER_BY;
    
    public function __construct(private readonly OrderByRequest $orderByRequest)
    {
    }
    
    public function snakeCase(bool $use = true): self
    {
        $this->useSnake = $use;
        return $this;
    }
    
    public function filter(array $fields = [], string $defaultOrderBy = OrderByRequest::DEFAULT_ORDER_BY): self
    {
        $this->filterFields = $fields;
        $this->defaultOrderBy = $defaultOrderBy;
        return $this;
    }
    
    public function set(Builder $builder): Builder
    {
        $by = $this->useSnake ? Str::snake($this->orderByRequest->getOrderBy()) : $this->orderByRequest->getOrderBy();
        if ($this->filterFields && !in_array($by, $this->filterFields, true)) {
            $by = $this->defaultOrderBy;
        }
        
        return $builder
            ->orderBy($by, $this->orderByRequest->getOrderDirection());
    }
}