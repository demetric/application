<?php

namespace App\Components\Dto;

use Carbon\Carbon;

trait BaseField
{
    use IdField;
    use DateField;
}