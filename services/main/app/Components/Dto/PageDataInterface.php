<?php

namespace App\Components\Dto;

interface PageDataInterface
{
    public function getPage(): int;
    
    public function getPerPage(): int;
}