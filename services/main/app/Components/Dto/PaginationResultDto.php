<?php

namespace App\Components\Dto;

use App\Components\BaseDtoObject;

class PaginationResultDto extends BaseDtoObject implements PageDataInterface
{
    private int $count;
    private int $page;
    private int $perPage;
    
    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }
    
    /**
     * @param int $count
     * @return PaginationResultDto
     */
    public function setCount(int $count): PaginationResultDto
    {
        $this->count = $count;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }
    
    /**
     * @param int $page
     * @return PaginationResultDto
     */
    public function setPage(int $page): PaginationResultDto
    {
        $this->page = $page;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }
    
    /**
     * @param int $perPage
     * @return PaginationResultDto
     */
    public function setPerPage(int $perPage): PaginationResultDto
    {
        $this->perPage = $perPage;
        return $this;
    }
}