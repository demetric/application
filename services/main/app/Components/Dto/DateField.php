<?php

namespace App\Components\Dto;

use Carbon\Carbon;

trait DateField
{
    private Carbon $createdAt;
    private Carbon $updatedAt;
    
    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon
    {
        return $this->createdAt;
    }
    
    /**
     * @param Carbon $createdAt
     * @return static
     */
    public function setCreatedAt(Carbon $createdAt): static
    {
        $this->createdAt = $createdAt;
        return $this;
    }
    
    /**
     * @return Carbon
     */
    public function getUpdatedAt(): Carbon
    {
        return $this->updatedAt;
    }
    
    /**
     * @param Carbon $updatedAt
     * @return static
     */
    public function setUpdatedAt(Carbon $updatedAt): static
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}