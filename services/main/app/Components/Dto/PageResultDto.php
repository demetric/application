<?php

namespace App\Components\Dto;

use App\Components\BaseDtoObject;

class PageResultDto extends BaseDtoObject
{
    private array               $list;
    private PaginationResultDto $pagination;
    
    /**
     * @return array
     */
    public function getList(): array
    {
        return $this->list;
    }
    
    /**
     * @param array $list
     * @return PageResultDto
     */
    public function setList(array $list): PageResultDto
    {
        $this->list = $list;
        return $this;
    }
    
    /**
     * @return PaginationResultDto
     */
    public function getPagination(): PaginationResultDto
    {
        return $this->pagination;
    }
    
    /**
     * @param PaginationResultDto $pagination
     * @return PageResultDto
     */
    public function setPagination(PaginationResultDto $pagination): PageResultDto
    {
        $this->pagination = $pagination;
        return $this;
    }
    
}