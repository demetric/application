<?php

namespace App\Components\Validators;

use App\Components\DtoReflectionHelper;
use App\Components\Validators\Exceptions\ValidationException;
use ReflectionObject;
use Throwable;

class DtoValidator
{
    private const M_PRESENCE         = 'Field must be available';
    private const M_NOT_EMPTY        = 'Field cannot be empty';
    private const M_MIN_ONE_PRESENCE = 'At least one field must be';
    
    private const T_STRING = 'string';
    
    public function checkAll($dto, bool $validateMinOneAvailable = false, bool $validateAvailable = true): array
    {
        $isNotAvailable = false;
        $isAvailable = false;
        $errors = [];
        $reflectionDto = new ReflectionObject($dto);
        foreach ($reflectionDto->getMethods() as $inputMethod) {
            $getterName = DtoReflectionHelper::getter($inputMethod);
            if ($getterName) {
                $getterName = lcfirst($getterName);
                $methodName = $inputMethod->getName();
                try {
                    $value = $dto->$methodName();
                    $isAvailable = true;
                    $type = $inputMethod->getReturnType();
                    if ($type && $inputMethod->hasReturnType() && $type->isBuiltin() && $type->getName() === self::T_STRING && !trim($value)) {
                        $errors[$getterName] = self::M_NOT_EMPTY;
                    }
                } catch (Throwable) {
                    $isNotAvailable = true;
                    if ($validateAvailable && !$validateMinOneAvailable) {
                        $errors[$getterName] = self::M_PRESENCE;
                    }
                }
            }
        }
        if (!$isAvailable && $isNotAvailable && $validateMinOneAvailable) {
            $errors['*'] = self::M_MIN_ONE_PRESENCE;
        }
        return $errors;
    }
    
    /**
     * @param      $dto
     * @param bool $validateAvailable
     * @param bool $validateMinOneAvailable
     * @return void
     */
    public function checkAllAndThrow($dto, bool $validateMinOneAvailable = false, bool $validateAvailable = true): void
    {
        $errors = $this->checkAll($dto, $validateMinOneAvailable, $validateAvailable);
        if ($errors) {
            throw new ValidationException($errors);
        }
    }
}