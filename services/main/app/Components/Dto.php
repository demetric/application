<?php

namespace App\Components;

use App\Components\Types\Json;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Str;
use JetBrains\PhpStorm\ArrayShape;
use ReflectionObject;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Throwable;

abstract class Dto
{
    #[ArrayShape([Carbon::class => Closure::class, Json::class => Closure::class])]
    public static function getDefaultAutoHintObjectList(): array
    {
        return [
            Carbon::class => static function ($value) {
                return $value instanceof Carbon ? $value : Carbon::make($value);
            },
            Json::class => static function ($value) {
                return $value instanceof Json ? $value : new Json($value);
            },
        ];
    }
    
    public static function makeFromDataList($dataList, $className, ?array $autoHintObjectList = null): array
    {
        $list = [];
        foreach ($dataList as $item) {
            $list[] = self::makeFromDataItem($item, $className, $autoHintObjectList);
        }
        return $list;
    }
    
    public static function makeFromDataItem($dataItem, $className, ?array $autoHintObjectList = null)
    {
        $dto = is_string($className) ? new $className : $className;
        $reflectionObject = new ReflectionObject($dto);
        foreach ($dataItem as $key => $value) {
            $camelKey = Str::ucfirst(Str::camel($key));
            try {
                self::setSetter($reflectionObject, $camelKey, $dto, $value, $autoHintObjectList);
            } catch (Throwable) {
                continue;
            }
        }
        
        return $dto;
    }
    
    public static function fill($input, $output, ?array $autoHintObjectList = null)
    {
        $reflectionObjectInput = new ReflectionObject($input);
        $reflectionObjectOutput = new ReflectionObject($output);
        foreach ($reflectionObjectInput->getMethods() as $inputMethod) {
            $getterName = DtoReflectionHelper::getter($inputMethod);
            if ($getterName) {
                try {
                    self::setSetter($reflectionObjectOutput, $getterName, $output, $input->{$inputMethod->getName()}(), $autoHintObjectList);
                } catch (Throwable) {
                    continue;
                }
            }
        }
        
        return $output;
    }
    
    /**
     * @throws ExceptionInterface
     */
    public static function toArray($dto): array
    {
        return (new Serializer([new ObjectNormalizer()]))->normalize($dto);
    }
    
    /**
     * @throws ExceptionInterface
     */
    public static function toSnakeArray($dto): array
    {
        return (new Serializer([new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter())]))->normalize($dto);
    }
    
    private static function setSetter(ReflectionObject $reflectionObjectOutput, string $getterName, $object, $value, ?array $autoHintObjectList = null): void
    {
        $autoHintObjectList = $autoHintObjectList ?? self::getDefaultAutoHintObjectList();
        
        foreach ($reflectionObjectOutput->getMethods() as $outputMethod) {
            $setterName = DtoReflectionHelper::setter($outputMethod);
            if ($setterName && $getterName === $setterName) {
                if ($outputMethod->getParameters()[0]?->getType()?->isBuiltin() === false) {
                    $autoHintType = $outputMethod->getParameters()[0]?->getType()?->getName();
                    if (array_key_exists($autoHintType, $autoHintObjectList)) {
                        $value = $autoHintObjectList[$autoHintType]($value);
                    } else {
                        $value = self::makeFromDataItem($value, $autoHintType, $autoHintObjectList);
                    }
                }
                $object->{$outputMethod->getName()}($value);
                return;
            }
        }
    }
  
}