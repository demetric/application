<?php

namespace App\Components;

abstract class BaseDtoObject
{
    public static function make(): static
    {
        return new static();
    }
}