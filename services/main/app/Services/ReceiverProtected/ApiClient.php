<?php

namespace App\Services\ReceiverProtected;

use GuzzleHttp\Client;

class ApiClient
{
    private Client $client;
    
    public function __construct()
    {
        $this->client = new Client(['base_uri' => env('RECEIVER_PROTECTED_SERVICE_URL')]);
    }
    
    public function getClient(): Client
    {
        return $this->client;
    }
}