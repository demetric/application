<?php

namespace App\Services\ReceiverProtected;

use App\Services\ReceiverProtected\ApiEntities\Cache;
use GuzzleHttp\Client;
use Illuminate\Contracts\Container\BindingResolutionException;

class ReceiverProtectedManager
{
    private Client $client;
    
    public function __construct(ApiClient $client)
    {
        $this->client = $client->getClient();
    }
    
    /**
     * @throws BindingResolutionException
     */
    public function cache(): Cache
    {
        return app()->make(Cache::class, $this->getApiEntityDepends());
    }
    
    private function getApiEntityDepends(): array
    {
        return [
            'client' => $this->client,
        ];
    }
}