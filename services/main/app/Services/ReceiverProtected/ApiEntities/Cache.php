<?php

namespace App\Services\ReceiverProtected\ApiEntities;

use GuzzleHttp\Exception\GuzzleException;

class Cache extends BaseApiEntity
{
    /**
     * @throws GuzzleException
     */
    public function flushall(): void
    {
        $this->client->get('cache/flushall');
    }
}