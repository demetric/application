<?php

namespace App\Services\ReceiverProtected\ApiEntities;

use GuzzleHttp\Client;

abstract class BaseApiEntity
{
    public function __construct(protected Client $client)
    {
    
    }
}