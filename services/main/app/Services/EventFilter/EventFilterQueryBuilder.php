<?php

namespace App\Services\EventFilter;

use App\Components\Types\Json;
use App\Services\EventFilter\Exceptions\UndefinedConditionException;
use App\Services\EventFilter\Exceptions\UndefinedFunctionException;
use App\Services\EventFilter\Exceptions\UndefinedGroupConditionException;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use JsonException;

class EventFilterQueryBuilder
{
    private array $body;
    private array $existsPathIds;
    private const AS_PREFIX    = 'ei';
    private const BASE_COLUMNS = [
        'id' => 'id',
        'createdAt' => 'created_at',
        'appId' => 'app_id',
        'body' => 'body',
    ];
    
    /**
     * @throws JsonException
     */
    public function __construct()
    {
        $this->body = [];
        $filter = Db::table('event_filter')->limit(1)->get()->first();
        if ($filter && $filter->active) {
            $this->body = (new Json($filter->body))->decode();
        }
        $this->existsPathIds = DB::table('event_index_path')->get('id')->pluck('id')->toArray();
    }
    
    /**
     * @param Builder $builder
     * @return Builder
     * @throws UndefinedConditionException
     * @throws UndefinedFunctionException
     * @throws UndefinedGroupConditionException
     */
    public function buildQuery(Builder $builder): Builder
    {
        $columnIds = $this->getJoinColumnIds($builder, $this->body);
        foreach ($columnIds as $columnId) {
            $as = self::AS_PREFIX . $columnId;
            $builder->leftJoin(DB::raw("event_index as $as"),
                function (Builder $builder) use ($as, $columnId) {
                    /** @noinspection PhpPossiblePolymorphicInvocationInspection */
                    $builder->on('event.id', '=', "$as.event_id");
                    /** @noinspection PhpPossiblePolymorphicInvocationInspection */
                    $builder->on("$as.path_id", '=', DB::raw($columnId));
                }
            );
        }
        $_this = $this;
        $builder->where(function (Builder $builder) use ($_this) {
            $_this->build($builder, $this->body);
        });

        return $builder;
    }
    
    private function getJoinColumnIds(Builder $builder, array $items): array
    {
        $columns = [];
        foreach ($items as $item) {
            if (array_key_exists('func', $item) && !array_key_exists($item['columnId'], self::BASE_COLUMNS)) {
                if ($this->existsPathColumn($item['columnId'])) {
                    $columns[] = $item['columnId'];
                }
            } elseif (array_key_exists('data', $item)) {
                $columns = [...$columns, ...$this->getJoinColumnIds($builder, $item['data'])];
            }
        }
        
        return array_unique($columns);
    }
    
    /**
     * @param Builder $builder
     * @param array   $items
     * @return void
     * @throws UndefinedConditionException
     * @throws UndefinedGroupConditionException
     * @throws UndefinedFunctionException
     */
    private function build(Builder $builder, array $items): void
    {
        $isFirst = false;
        foreach ($items as $item) {
            if (!(isset($item['active']) && $item['active'] === true)) {
                continue;
            }
            if (!$isFirst) {
                $builder->whereRaw('1');
                $isFirst = true;
            }
            if (array_key_exists('func', $item)) {
                $this->makeFunction($builder, $item);
            } else {
                switch ($item['cond']) {
                    case 'or':
                        $builder->orWhere(function (Builder $builder) use ($item) {
                            $this->build($builder, $item['data']);
                        });
                        break;
                    case 'and':
                        $builder->where(function (Builder $builder) use ($item) {
                            $this->build($builder, $item['data']);
                        });
                        break;
                    default:
                        throw new UndefinedGroupConditionException($item);
                }
            }
        }
    }
    
    /**
     * @param Builder $builder
     * @param array   $item
     * @return void
     * @throws UndefinedConditionException
     * @throws UndefinedFunctionException
     */
    private function makeFunction(Builder $builder, array $item): void
    {
        $value = $item['value'];
        $column = $item['columnId'];
        $originColumn = null;
        if (!array_key_exists($column, self::BASE_COLUMNS)) {
            if ($this->existsPathColumn($column)) {
                $originColumn = self::AS_PREFIX . $column . '.origin';
                $column = self::AS_PREFIX . $column . '.body';
            }
        } else {
            $column = 'event.' . self::BASE_COLUMNS[$item['columnId']];
            if ($item['columnId'] === 'body') {
                $originColumn = 'event.origin';
            } else {
                $originColumn = $column;
            }
        }
        
        $negative = false;
        switch ($item['func']) {
            case 'contains':
                $operator = 'like';
                $value = "%$value%";
                break;
            case 'containsNot':
                $operator = 'not like';
                $value = "%$value%";
                $negative = true;
                break;
            case 'startsWith':
                $operator = 'like';
                $value = "$value%";
                break;
            case 'startsWithNot':
                $operator = 'not like';
                $value = "$value%";
                $negative = true;
                break;
            case 'endsWith':
                $operator = 'like';
                $value = "%$value";
                break;
            case 'endsWithNot':
                $operator = 'not like';
                $value = "%$value";
                $negative = true;
                break;
            case 'eq':
                $operator = '=';
                break;
            case 'ne':
                $operator = '<>';
                $negative = true;
                break;
            case 'lt':
                $operator = '<';
                break;
            case 'gt':
                $operator = '>';
                break;
            case 'le':
                $operator = '<=';
                break;
            case 'ge':
                $operator = '>=';
                break;
            default:
                throw new UndefinedFunctionException($item);
        }
        
        switch ($item['cond']) {
            case 'or':
                $this->buildFunction($builder, true, $column, $operator, $negative, $value, $originColumn);
                break;
            case 'and':
                $this->buildFunction($builder, false, $column, $operator, $negative, $value, $originColumn);
                break;
            default:
                throw new UndefinedConditionException($item);
        }
    }
    
    
    private function buildFunction(Builder $builder, bool $isOr, mixed $column, string $operator, bool $negative, mixed $value, ?string $originColumn): void
    {
        if (is_numeric($value)) {
            $this->buildWithNegativeNull($builder, $isOr, $column, $operator, +$value, $negative);
        } else {
            $this->buildWithNegativeNull($builder, $isOr, $originColumn, $operator, $value, $negative);
            
        }
    }
    
    private function buildWithNegativeNull(Builder $builder, bool $isOr, mixed $column, string $operator, mixed $value, bool $negative): void
    {
        if ($negative && $value) {
            $builder->where(function (Builder $builder) use ($column, $operator, $value, $isOr, $negative) {
                $this->buildWithEmptyNull($builder, $isOr, $column, $operator, $value, $negative)->orWhereNull($column);
            });
        } else {
            $this->buildWithEmptyNull($builder, $isOr, $column, $operator, $value, $negative);
        }
    }
    
    private function buildWithEmptyNull(Builder $builder, bool $isOr, mixed $column, string $operator, mixed $value, bool $negative): Builder
    {
        if (!$value) {
            $subCondition = function (Builder $builder) use ($column, $operator, $value, $negative) {
                if ($negative) {
                    $builder->where($column, $operator, $value)->orWhereNotNull($column);
                } else {
                    $builder->where($column, $operator, $value)->orWhereNull($column);
                }
            };
            if ($isOr) {
                $builder->orWhere($subCondition);
            } else {
                $builder->where($subCondition);
            }
        } elseif ($isOr) {
            $builder->orWhere($column, $operator, $value);
        } else {
            $builder->where($column, $operator, $value);
        }
        
        return $builder;
    }
    
    private function existsPathColumn(int $id): bool
    {
        return in_array($id, $this->existsPathIds, true);
    }
}