<?php

namespace App\Services\EventFilter\Exceptions;

use Exception;

class UndefinedConditionException extends Exception
{
    public function __construct(array $item)
    {
        parent::__construct("Undefined condition {$item['cond']} in uid {$item['uid']}");
    }
}