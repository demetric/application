<?php

namespace App\Services\EventFilter\Exceptions;

use Exception;

class UndefinedFunctionException extends Exception
{
    public function __construct(array $item)
    {
        parent::__construct("Undefined function {$item['func']} in uid {$item['uid']}");
    }
}