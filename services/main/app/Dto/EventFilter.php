<?php

namespace App\Dto;

use App\Components\Dto\BaseField;

class EventFilter extends NewEventFilter
{
  use BaseField;
}