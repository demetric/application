<?php

namespace App\Dto;

use App\Components\BaseDtoObject;
use App\Components\Types\Json;

class NewEventFilter extends BaseDtoObject
{
    private string $name;
    private Json   $body;
    private int    $sort;
    private bool   $active;
    
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    /**
     * @param string $name
     * @return NewEventFilter
     */
    public function setName(string $name): NewEventFilter
    {
        $this->name = $name;
        return $this;
    }
    
    /**
     * @return Json
     */
    public function getBody(): Json
    {
        return $this->body;
    }
    
    /**
     * @param Json $body
     * @return NewEventFilter
     */
    public function setBody(Json $body): NewEventFilter
    {
        $this->body = $body;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }
    
    /**
     * @param int $sort
     * @return NewEventFilter
     */
    public function setSort(int $sort): NewEventFilter
    {
        $this->sort = $sort;
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }
    
    /**
     * @param bool $active
     * @return NewEventFilter
     */
    public function setActive(bool $active): NewEventFilter
    {
        $this->active = $active;
        return $this;
    }
}