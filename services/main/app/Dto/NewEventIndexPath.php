<?php

namespace App\Dto;

use App\Components\BaseDtoObject;

class NewEventIndexPath extends BaseDtoObject
{
    private string $path;
    private string $altName;
    private bool   $isHidden;
    private int    $sort;
    
    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }
    
    /**
     * @param string $path
     * @return static
     */
    public function setPath(string $path): static
    {
        $this->path = $path;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getAltName(): string
    {
        return $this->altName;
    }
    
    /**
     * @param string $altName
     * @return static
     */
    public function setAltName(string $altName): static
    {
        $this->altName = $altName;
        return $this;
    }
    
    /**
     * @return bool
     */
    public function getIsHidden(): bool
    {
        return $this->isHidden;
    }
    
    /**
     * @param bool $isHidden
     * @return NewEventIndexPath
     */
    public function setIsHidden(bool $isHidden): NewEventIndexPath
    {
        $this->isHidden = $isHidden;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }
    
    /**
     * @param int $sort
     * @return NewEventIndexPath
     */
    public function setSort(int $sort): NewEventIndexPath
    {
        $this->sort = $sort;
        return $this;
    }
}