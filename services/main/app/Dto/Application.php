<?php

namespace App\Dto;

use App\Components\BaseDtoObject;

class Application extends BaseDtoObject
{
    private int     $id;
    private string  $name;
    private Project $project;
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    /**
     * @param int $id
     * @return Application
     */
    public function setId(int $id): Application
    {
        $this->id = $id;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    /**
     * @param string $name
     * @return Application
     */
    public function setName(string $name): Application
    {
        $this->name = $name;
        return $this;
    }
    
    /**
     * @return Project
     */
    public function getProject(): Project
    {
        return $this->project;
    }
    
    /**
     * @param Project $project
     * @return Application
     */
    public function setProject(Project $project): Application
    {
        $this->project = $project;
        return $this;
    }
}