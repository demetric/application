<?php

namespace App\Dto;

use App\Components\BaseDtoObject;
use App\Components\Types\Json;
use Carbon\Carbon;

class Event extends BaseDtoObject
{
    private int    $id;
    private int    $appId;
    private bool   $read;
    private Carbon $createdAt;
    private Carbon $updatedAt;
    private ?Json  $body = null;
    /**
     * @var EventIndex[]
     */
    private array $eventIndexList;
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    /**
     * @param int $id
     * @return Event
     */
    public function setId(int $id): Event
    {
        $this->id = $id;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getAppId(): int
    {
        return $this->appId;
    }
    
    /**
     * @param int $appId
     * @return Event
     */
    public function setAppId(int $appId): Event
    {
        $this->appId = $appId;
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isRead(): bool
    {
        return $this->read;
    }
    
    /**
     * @param bool $read
     * @return Event
     */
    public function setRead(bool $read): Event
    {
        $this->read = $read;
        return $this;
    }
    
    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon
    {
        return $this->createdAt;
    }
    
    /**
     * @param Carbon $createdAt
     * @return Event
     */
    public function setCreatedAt(Carbon $createdAt): Event
    {
        $this->createdAt = $createdAt;
        return $this;
    }
    
    /**
     * @return Carbon
     */
    public function getUpdatedAt(): Carbon
    {
        return $this->updatedAt;
    }
    
    /**
     * @param Carbon $updatedAt
     * @return Event
     */
    public function setUpdatedAt(Carbon $updatedAt): Event
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
    
    /**
     * @return Json|null
     */
    public function getBody(): ?Json
    {
        return $this->body;
    }
    
    /**
     * @param Json|null $body
     * @return Event
     */
    public function setBody(?Json $body): Event
    {
        $this->body = $body;
        return $this;
    }
    
    /**
     * @return EventIndex[]
     */
    public function getEventIndexList(): array
    {
        return $this->eventIndexList;
    }
    
    /**
     * @param EventIndex[] $eventIndexList
     * @return Event
     */
    public function setEventIndexList(array $eventIndexList): Event
    {
        $this->eventIndexList = $eventIndexList;
        return $this;
    }
}