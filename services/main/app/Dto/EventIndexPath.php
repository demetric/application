<?php

namespace App\Dto;

use App\Components\Dto\BaseField;

class EventIndexPath extends NewEventIndexPath
{
    use BaseField;
}