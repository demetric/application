<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;

use App\Components\Serializer\Serializer;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    /**
     * Controller constructor.
     *
     * @param Serializer $serializer
     */
    public function __construct(protected Serializer $serializer)
    {
    }
}
