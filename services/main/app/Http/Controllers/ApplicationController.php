<?php

namespace App\Http\Controllers;

use App\Components\Dto;
use App\Dto\Application;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class ApplicationController extends Controller
{
    public function getList(): Response
    {
        $query = DB::table('application')
            ->addSelect('id')
            ->addSelect('name')
            ->addSelect('project_id');
        
        $applications = $query->get();
        if ($applications->count()) {
            $projectIds = array_column($applications->toArray(), 'project_id');
            $queryProject = DB::table('project')
                ->addSelect('id')
                ->addSelect('name')
                ->whereIn('id', $projectIds);
            $projects = $queryProject->get()->pluck([], 'id');
            foreach ($applications as $application) {
                $application->project = $projects->get($application->project_id);
            }
        }
   
        return $this->serializer->response([
            'list' => Dto::makeFromDataList($applications, Application::class),
        ]);
    }
}