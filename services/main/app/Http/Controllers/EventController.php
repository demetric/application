<?php

namespace App\Http\Controllers;

use App\Repositories\EventRepository;
use Symfony\Component\HttpFoundation\Response;

class EventController extends Controller
{
    public function getList(EventRepository $eventRepository): Response
    {
        return $this->serializer->listChunkResponse($eventRepository);
    }
    
    public function truncate(EventRepository $eventRepository): Response
    {
        $eventRepository->truncate();
        return new Response();
    }
}