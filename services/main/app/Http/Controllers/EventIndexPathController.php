<?php

namespace App\Http\Controllers;

use App\Components\Repository\Exceptions\NotFoundException;
use App\Http\Requests\NewEventIndexPathRequest;
use App\Repositories\EventIndexPathRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class EventIndexPathController extends Controller
{
    public function getList(EventIndexPathRepository $eventIndexPathRepository): Response
    {
        return $this->serializer->listChunkResponse($eventIndexPathRepository);
    }
    
    /**
     * @throws NotFoundException|Throwable
     */
    public function create(EventIndexPathRepository $eventIndexPathRepository, NewEventIndexPathRequest $request): Response
    {
        return $this->serializer->response($eventIndexPathRepository->create($request));
    }
    
    public function find(EventIndexPathRepository $eventIndexPathRepository, int $id): Response
    {
        try {
            return $this->serializer->response($eventIndexPathRepository->find($id));
        } catch (NotFoundException) {
            throw new NotFoundHttpException();
        }
    }
    
    /**
     * @throws Throwable
     */
    public function update(EventIndexPathRepository $eventIndexPathRepository, NewEventIndexPathRequest $request, int $id): Response
    {
        try {
            $eventIndexPathRepository->update($id, $request);
            return $this->serializer->response($eventIndexPathRepository->find($id));
        } catch (NotFoundException) {
            throw new NotFoundHttpException();
        }
    }
    
    /**
     * @throws Throwable
     */
    public function delete(EventIndexPathRepository $eventIndexPathRepository, int $id): Response
    {
        try {
            $eventIndexPathRepository->delete($id);
            return $this->serializer->responseOneField($id);
        } catch (NotFoundException) {
            throw new NotFoundHttpException();
        }
    }
}