<?php

namespace App\Http\Controllers;

use App\Components\Dto;
use App\Dto\EventFilter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use JsonException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class EventFilterController extends Controller
{
    public function get(): Response
    {
        $filter = Db::table('event_filter')->limit(1)->get()->first();
        if (!$filter) {
            $filter = [];
        }
        return $this->serializer->response(Dto::makeFromDataItem($filter, EventFilter::class));
    }
    
    /**
     * @throws JsonException|ExceptionInterface
     */
    public function save(Request $request): Response
    {
        $filterData = $request->all();
        $filter = Db::table('event_filter')->limit(1)->get()->first();
        $data = [];
        if ($filter) {
            $id = $filter->id;
            if ($filterData) {
                $data = [
                    ...Dto::toArray($filter),
                    'body' => json_encode($filterData, JSON_THROW_ON_ERROR),
                    'updated_at' => Carbon::now(),
                ];
                $affectedId = Db::table('event_filter')->where('id', $id)->update($data);
            } else {
                $affectedId = Db::table('event_filter')->delete($id);
            }
        } else {
            $data = [
                'name' => 'Default',
                'body' => json_encode($filterData, JSON_THROW_ON_ERROR),
                'sort' => 100,
                'active' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
            $affectedId = Db::table('event_filter')->insertGetId($data);
        }
        
        if (!$affectedId) {
            throw new ServiceUnavailableHttpException();
        }
        
        $data = ['id' => $affectedId, ...$data];
        
        return $this->serializer->response(Dto::makeFromDataItem($data, EventFilter::class));
    }
    
    /**
     * @param Request      $request
     * @return Response
     * @throws ExceptionInterface
     */
    public function activate(Request $request): Response
    {
        $active = (bool)$request->post('active');
        $filter = Db::table('event_filter')->limit(1)->get()->first();
        if (!$filter) {
            throw new NotFoundHttpException();
        }
        
        $data = [
            ...Dto::toArray($filter),
            'active' => $active,
            'updated_at' => Carbon::now(),
        ];
        
        $affectedId = Db::table('event_filter')->where('id', $filter->id)->update($data);
        if (!$affectedId) {
            throw new ServiceUnavailableHttpException();
        }
        
        return $this->serializer->response(Dto::makeFromDataItem($data, EventFilter::class));
    }
}