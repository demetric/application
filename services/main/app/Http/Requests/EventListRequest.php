<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class EventListRequest
{
    /**
     * @var int[]
     */
    private array $appId;
    
    public function __construct(Request $request)
    {
        $all = $request->all();
        $paramAppId = trim((string)Arr::get($all, 'appId'));
        $appId = $paramAppId ? explode(',', $paramAppId) : [];
        foreach ($appId as $key => $value) {
            $appId[$key] = (int)trim($value);
        }
        $this->appId = $appId;
    }
    
    /**
     * @return array
     */
    public function getAppId(): array
    {
        return $this->appId;
    }
}