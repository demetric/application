<?php

namespace App\Http\Requests;

use App\Components\Dto;
use App\Components\Validators\DtoValidator;
use App\Dto\NewEventIndexPath;
use Illuminate\Http\Request;

class NewEventIndexPathRequest extends NewEventIndexPath
{
    public function __construct(Request $request, DtoValidator $validator)
    {
        Dto::makeFromDataItem($request->all(), $this);
        $validator->checkAllAndThrow($this, $request->method() === 'PATCH');
    }
}