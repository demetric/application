<?php

use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\EventFilterController;
use App\Http\Controllers\EventIndexPathController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::name('Список приложений')
    ->get('/application', [ApplicationController::class, 'getList']);
Route::name('Список событий')
    ->get('/event', [EventController::class, 'getList']);
Route::name('Очистить все логи')
    ->post('/event/truncate', [EventController::class, 'truncate']);
Route::name('Список путей для индекса')
    ->get('/event-index-path', [EventIndexPathController::class, 'getList']);
Route::name('Создать путь индекса')
    ->post('/event-index-path', [EventIndexPathController::class, 'create']);
Route::name('Получить путь индекса')
    ->get('/event-index-path/{id}', [EventIndexPathController::class, 'find']);
Route::name('Обновить путь индекса')
    ->put('/event-index-path/{id}', [EventIndexPathController::class, 'update']);
Route::name('Обновить поле в пути индекса')
    ->patch('/event-index-path/{id}', [EventIndexPathController::class, 'update']);
Route::name('Удалить путь индекса')
    ->delete('/event-index-path/{id}', [EventIndexPathController::class, 'delete']);
Route::name('Получить фильтр')
    ->get('/event-filter', [EventFilterController::class, 'get']);
Route::name('Сохранить фильтр')
    ->post('/event-filter', [EventFilterController::class, 'save']);
Route::name('Управление активностью фильтра')
    ->post('/event-filter/activate', [EventFilterController::class, 'activate']);