import {createSlice} from '@reduxjs/toolkit'

export const eventTruncateSlice = createSlice({
    name: 'eventTruncate',
    initialState: {
        loading: false,
        error: false,
    },
    reducers: {
        truncating: (state) => {
            state.loading = true;
            state.error = false;
        },
        truncated: (state) => {
            state.loading = false;
            state.error = false;
        },
        error: (state) => {
            state.loading = false;
            state.error = true;
        },
    },
})


export const {truncating, truncated, error} = eventTruncateSlice.actions

export default eventTruncateSlice.reducer