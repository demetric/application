import {createSlice} from '@reduxjs/toolkit'
import checkAllowByFilter, {LinearEvent} from "../tools/checkAllowByFilter";
import {DataTypeTree} from "../components/FilterModal/types";


export const eventSlice = createSlice({
    name: 'event',
    initialState: {
        isLoaded: false,
        streamActive: Number(localStorage.getItem('streamActive')),
        pagination: {
            count: 0,
            page: 1,
            perPage: 10,
        },
        list: [],
        filter: {},
        customFilter: [] as DataTypeTree,
    },
    reducers: {
        setLoaded: (state, action) => {
            // @ts-ignore
            state.isLoaded = action.payload;
        },
        setStreamActive: (state, action) => {
            // @ts-ignore
            state.streamActive = action.payload;
            localStorage.setItem('streamActive', String(Number(action.payload)));
        },
        unshift: (state, action) => {
            // @ts-ignore
            state.list.unshift(action.payload);
        },
        push: (state, action) => {
            // @ts-ignore
            state.list.push(action.payload);
        },
        setList: (state, action) => {
            // @ts-ignore
            state.list = [...action.payload];
        },
        setPagination: (state, action) => {
            // @ts-ignore
            state.pagination = {...action.payload};
        },
        unshiftOnline: (state, action) => {
            if (state.pagination.page < 2 && state.isLoaded && state.streamActive && allowPush(action.payload, state.filter, state.customFilter)) {
                // @ts-ignore
                state.list.unshift(action.payload);
                state.list = state.list.slice(0, state.pagination.perPage);
                state.pagination.count = state.pagination.count + 1;
            }
        },
        setFilter: (state, action) => {
            // @ts-ignore
            state.filter = {...action.payload};
        },
        setCustomFilter: (state, action: { payload: DataTypeTree }) => {
            state.customFilter = action.payload;
        }
    },
})

const allowPush = (data: any, filter: any, customFilter: DataTypeTree): boolean => {
    const checkByFilter = (data: any): boolean => {
        const linearEvent: LinearEvent = {
            id: data.id,
            createdAt: data.createdAt,
            appId: data.appId,
            body: data.body,
        };
        for (const eventIndex of data.eventIndexList ?? []) {
            linearEvent[eventIndex.pathId] = eventIndex.body;
        }
        return checkAllowByFilter(linearEvent, customFilter);
    }

    const appId: number[] = filter?.appId ?? [];
    if ((appId.length === 0 || appId.includes(data.appId)) && checkByFilter(data)) {
        return true;
    }

    return false;

}

export const {setLoaded, setStreamActive, unshift, push, setList, setPagination, unshiftOnline, setFilter, setCustomFilter} = eventSlice.actions

export default eventSlice.reducer
