import {createSlice} from '@reduxjs/toolkit'
import {omit} from 'lodash';
import {DataTypeTree} from "../components/FilterModal/types";

export const filterSlice = createSlice({
    name: 'filterSlice',
    initialState: {
        isShowModal: false,
        dataNormalize: {} as dataNormalizeType,
        data: [] as DataTypeTree,
        active: false,
    },
    reducers: {
        showModal: (state, action) => {
            state.isShowModal = action.payload;
        },
        initDataNormalize: (state, action: { payload: dataNormalizeType }) => {
            state.dataNormalize = action.payload;
        },
        append: (state, action: { payload: appendNode }) => {
            const {data, parentUid} = action.payload;
            state.dataNormalize[action.payload.data.uid] = {data: omit(data, ['uid']), parentUid};
        },
        update: (state, action: { payload: node }) => {
            const {data} = action.payload;
            state.dataNormalize[action.payload.data.uid].data = omit(data, ['uid']);
        },
        remove: (state, action: { payload: string }) => {
            for (const stateElementKey in state.dataNormalize) {
                if (state.dataNormalize[stateElementKey].parentUid === action.payload) {
                    delete state.dataNormalize[stateElementKey];
                }
            }

            delete state.dataNormalize[action.payload];
            removeLostNodes(state.dataNormalize);
        },
        initData: (state, action: { payload: DataTypeTree }) => {
            state.data = action.payload;
        },
        setActive: (state, action: { payload: boolean }) => {
            state.active = action.payload;
        }
    },
})

const removeLostNodes = (dataNormalize: dataNormalizeType) => {
    let hasDeleted: boolean = false;
    for (const uid in dataNormalize) {
        if (dataNormalize[uid].parentUid && !dataNormalize.hasOwnProperty(dataNormalize[uid].parentUid as string)) {
            delete dataNormalize[uid];
            hasDeleted = true;
        }
    }
    if (hasDeleted) {
        removeLostNodes(dataNormalize);
    }
}

export type dataNormalizeType = {
    [key: string]: appendNode
}

type appendNode = node & {
    parentUid: string | null,
}


type node = {
    data: any | { uid: string, },
}

export const {showModal, initData, setActive, initDataNormalize, append, update, remove} = filterSlice.actions

export default filterSlice.reducer