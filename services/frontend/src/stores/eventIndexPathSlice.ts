import {createSlice} from '@reduxjs/toolkit'


export const eventIndexPathSlice = createSlice({
    name: 'eventIndexPath',
    initialState: {
        pagination: {
            count: 0,
            page: 1,
            perPage: 10,
        },
        list: []
    },
    reducers: {
        setList: (state, action) => {
            // @ts-ignore
            state.list = [...action.payload];
        },
        setPagination: (state, action) => {
            // @ts-ignore
            state.pagination = {...action.payload};
        },
    },
})

export const {setList, setPagination} = eventIndexPathSlice.actions

export default eventIndexPathSlice.reducer