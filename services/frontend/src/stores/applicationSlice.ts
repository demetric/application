import {createSlice} from '@reduxjs/toolkit'


export const applicationSlice = createSlice({
    name: 'application',
    initialState: {
        list: []
    },
    reducers: {
        setList: (state, action) => {
            // @ts-ignore
            state.list = [...action.payload];
        },
    },
})

export const {setList} = applicationSlice.actions

export default applicationSlice.reducer