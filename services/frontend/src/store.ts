import {configureStore} from '@reduxjs/toolkit'
import eventReducer from './stores/eventSlice'
import applicationSlice from "./stores/applicationSlice";
import eventIndexPathSlice from "./stores/eventIndexPathSlice";
import eventTruncateSlice from "./stores/eventTruncateSlice";
import filterSlice from "./stores/filterSlice";

export default configureStore({
    reducer: {

        event: eventReducer,
        eventTruncate: eventTruncateSlice,
        application: applicationSlice,
        eventIndexPath: eventIndexPathSlice,
        filter: filterSlice,
    },
})