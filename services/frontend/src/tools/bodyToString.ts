import {isObject} from "lodash";

export default function (body: any): string {
    body = isObject(body) ? JSON.stringify(body) : body;
    (body as string) = body ? String(body) : '';
    return body;
}