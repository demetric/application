import {toInteger} from "lodash";

export function toArrNumberFromUrl(query: URLSearchParams, field: string, split = ','): number[] {
    return toArrNumber(query.get(field), split);
}

export function toArrNumber(value: any, split = ','): number[] {
    return value?.split(split).map((i: string) => toInteger(i.trim())) ?? []
}