export function find(array: any[], key: string, value: any, output: string | CallbackFunction | null = null): any | null {
    for (const arrayElement of array) {
        if (arrayElement.hasOwnProperty(key) && arrayElement[key] === value) {
            if (typeof output === 'function') {
                return output(arrayElement);
            }
            return (output !== null && arrayElement.hasOwnProperty(output)) ? arrayElement[output] : arrayElement;
        }
    }

    return null;
}
type CallbackFunction = (arrayElement: any) => any;