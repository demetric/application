import {DataTypeFunc, DataTypeGroup, DataTypeTree} from "../components/FilterModal/types";
import {FunctionTypes} from "../components/FilterModal/components/Function";
import bodyToString from "./bodyToString";

export default function (columns: LinearEvent, filter: DataTypeTree): boolean {
    return checkCondition(columns, filter);
}


const checkCondition = (columns: LinearEvent, arr: Array<DataTypeFunc | DataTypeGroup | { and: DataTypeTree, cond?: any, active: boolean }>): boolean => {
    arr = arr.filter(i => i.active);
    let result = !arr.length ? true : arr[0].cond === 'or';
    const orGroups = [];
    let andGroup = [];
    for (let i = 0; i < arr.length; i++) {
        if (arr[i]?.active !== true) {
            continue;
        }
        if (i + 1 < arr.length && arr[i + 1].cond === 'or') {
            if (andGroup.length) {
                andGroup.push(arr[i]);
                orGroups.push({
                    and: andGroup,
                    active: true,
                });
                andGroup = [];
            } else {
                orGroups.push(arr[i]);
            }
        } else if (i + 1 < arr.length && arr[i + 1].cond === 'and') {
            andGroup.push(arr[i]);
        } else {
            if (andGroup.length) {
                andGroup.push(arr[i]);
                orGroups.push({
                    and: andGroup,
                    active: true,
                });
                andGroup = [];
            } else {
                orGroups.push(arr[i]);
            }
        }
    }

    for (const orGroupElement of orGroups as Array<any>) {
        if (orGroupElement.hasOwnProperty('and')) {
            let res = true;
            for (const andGroupElement of orGroupElement.and) {
                if (andGroupElement.hasOwnProperty('data')) {
                    if (!checkCondition(columns, andGroupElement.data)) {
                        res = false;
                        break;
                    }
                } else if (!checkFunction(columns[andGroupElement.columnId] ?? null, andGroupElement.func, andGroupElement.value)) {
                    res = false;
                    break;
                }
            }
            if (res) {
                result = true;
                break;
            }
        } else {
            if (orGroupElement.hasOwnProperty('data')) {
                if (checkCondition(columns, orGroupElement.data)) {
                    result = true;
                    break;
                }
            } else if (checkFunction(columns[orGroupElement.columnId] ?? null, orGroupElement.func, orGroupElement.value)) {
                result = true;
                break;
            }
        }
    }

    return result;
}

const checkFunction = (value: any, func: string, target: any): boolean => {
    value = bodyToString(value);
    target = bodyToString(target);
    switch (func) {
        case FunctionTypes.contains:
            return value.includes(target);
        case FunctionTypes.containsNot:
            return !value.includes(target);
        case FunctionTypes.startsWith:
            return value.startsWith(target);
        case FunctionTypes.startsWithNot:
            return !value.startsWith(target);
        case FunctionTypes.endsWith:
            return value.endsWith(target);
        case FunctionTypes.endsWithNot:
            return !value.endsWith(target);
        case FunctionTypes.eq:
            return value === target;
        case FunctionTypes.ne:
            return value !== target;
        case FunctionTypes.lt:
            return value < target;
        case FunctionTypes.gt:
            return value > target;
        case FunctionTypes.le:
            return value <= target;
        case FunctionTypes.ge:
            return value >= target;
    }

    return true;
}

export type LinearEvent = {
    [key: string | number]: any
}