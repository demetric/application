import {useLocation} from 'react-router-dom';
import React from 'react';
import {useNavigate as reactRouterNavigate} from "react-router-dom";

function paramsToObject(entries: any): QueryParam {
    const queryParamObject: QueryParam = {}
    for (const [key, value] of entries) {
        queryParamObject[key] = value;
    }
    return queryParamObject;
}

export const useQuery = () => {
    const {search} = useLocation();

    return React.useMemo(() => new URLSearchParams(search), [search]);
}


export const useNavigate = () => {
    const reactNavigate = reactRouterNavigate();
    const query = useQuery();
    const navigate = (params: QueryParam, merge: boolean = true) => {
        const queryParamObject: QueryParam = merge ? paramsToObject(query.entries()) : {};
        for (const paramsKey in params) {
            if (!params[paramsKey]) {
                delete queryParamObject[paramsKey];
            } else {
                queryParamObject[paramsKey] = params[paramsKey];
            }
        }
        // @ts-ignore
        const queryString = new URLSearchParams(queryParamObject).toString();
        reactNavigate(queryString ? ('?' + queryString) : '');
    }
    return {navigate};
}

type QueryParam = {
    [name: string]: string | number | null | undefined;
}
