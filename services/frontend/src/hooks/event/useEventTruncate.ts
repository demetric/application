import {useDispatch, useSelector} from "react-redux";
import axios from "axios";
import {error as setError, truncated, truncating} from "../../stores/eventTruncateSlice";
import {isFunction} from "lodash";

export default function ({onSuccess}: self = {}) {
    const dispatch = useDispatch();
    const {loading, error} = useSelector((state: any) => {
        return state.eventTruncate
    });
    const eventTruncate = () => {
        dispatch(truncating());
        axios.post(
            'http://main.demetric.172.38.128.2.nip.io/event/truncate',
            {
                params: {}
            }
        ).then(function () {
            dispatch(truncated());
            if (isFunction(onSuccess)) {
                onSuccess();
            }
        }).catch(function () {
            dispatch(setError());
        });

    }

    return {eventTruncate, loading, error}
}

type self = {
    onSuccess?: () => any
}
