import {useSelector} from "react-redux";

export default function () {

    const indexList: Column[] = useSelector((state: any) => {
        return state.eventIndexPath.list
    });

    const columns: Column[] = [
        {
            id: 'id',
            altName: 'ID',
            base: true,
        },
        {
            id: 'createdAt',
            altName: 'Created At',
            base: true,
        },
        {
            id: 'appId',
            altName: 'App',
            base: true,
        },
    ];
    indexList.map(i => {
        columns.push({...i, base: false});
    });

    const columnsAfter: Column[] = [
        {
            id: 'body',
            altName: 'Body',
            base: true,
        }
    ];

    columns.push(...columnsAfter);

    return {columns}
}

export type Column = {
    id: string | number,
    altName: string,
    base: boolean,
};

