import {useDispatch, useSelector} from "react-redux";
import {useQuery} from "../useNavigator";
import {setFilter, setList, setLoaded, setPagination} from "../../stores/eventSlice";
import axios from "axios";
import {setList as setAppList} from "../../stores/applicationSlice";
import {setList as setEventIndexPathList,setPagination as setEventIndexPathPagination} from "../../stores/eventIndexPathSlice";
import {toArrNumber} from "../../tools/queryParamHelper";

function useList() {
    const pagination = useSelector((state: any) => {
        return state.event.pagination
    });
    const query = useQuery();
    const dispatch = useDispatch();
    const page = query.get('page');
    const perPage = query.get('perPage') || pagination.perPage;
    const appId = query.get('appId');
    const loadList = () => {
        dispatch(setLoaded(false));
        axios(
            'http://main.demetric.172.38.128.2.nip.io/application',
            {
                params: {}
            }
        ).then(function (response) {
            dispatch(setAppList(response.data.list));
        });
        dispatch(setLoaded(false));
        axios(
            'http://main.demetric.172.38.128.2.nip.io/event-index-path',
            {
                params: {
                    orderBy: 'sort',
                    orderDirection: 'asc',
                }
            }
        ).then(function (response) {
            dispatch(setEventIndexPathList(response.data.list));
            dispatch(setEventIndexPathPagination(response.data.pagination));
        });
        axios(
            'http://main.demetric.172.38.128.2.nip.io/event',
            {
                params: {
                    page,
                    perPage,
                    appId,
                }
            }
        ).then(function (response) {
            dispatch(setList(response.data.list));
            dispatch(setPagination(response.data.pagination));
            dispatch(setLoaded(true));
            dispatch(setFilter({appId: toArrNumber(appId)}));
        });
    }

    return {loadList}
}

export default useList;
