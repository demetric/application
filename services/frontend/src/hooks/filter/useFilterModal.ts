import {useDispatch, useSelector} from "react-redux";
import {showModal as showModalState} from "../../stores/filterSlice";

export default function () {
    const dispatch = useDispatch();
    const isShowModal = useSelector((state: any) => {
        return state.filter.isShowModal
    });
    const showModal = (show: boolean = true) => {
        dispatch(showModalState(show));
    }

    return {isShowModal, showModal}
}


