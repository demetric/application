import {useDispatch, useSelector} from "react-redux";
import {dataNormalizeType, initData, initDataNormalize, setActive} from "../../stores/filterSlice";
import {useState} from "react";
import axios from "axios";
import {DataTypeFunc, DataTypeTree} from "../../components/FilterModal/types";
import {foldAccordions, makeDataNormalize} from "../../components/FilterModal/tools";
import {setCustomFilter} from "../../stores/eventSlice";
import {find, remove} from "lodash";
import useColumn, {Column} from "../event/useColumn";
import {FunctionTypeNames} from "../../components/FilterModal/components/Function";

export default function () {

    const [loading, setLoading] = useState<boolean>(false);
    const [saving, setSaving] = useState<boolean>(false);
    const [error, setError] = useState<boolean>(false);

    const dispatch = useDispatch();
    const {data, active}: {
        data: DataTypeTree,
        active: boolean,
    } = useSelector((state: any) => {
        return state.filter
    });
    const {columns} = useColumn();
    const runtimeFilterColumns: Array<{
        altName: string,
        funcAltName: FunctionTypeNames,
        value: string,
    }> = [];
    const runtimeFilterFields = data.filter((i: any) => i?.runtime === true);
    const isActiveRuntimeFilter = !!runtimeFilterFields.length;
    for (const runtimeFilterField of runtimeFilterFields as Array<DataTypeFunc>) {
        runtimeFilterColumns.push({
            altName: find(columns, (i: Column) => i.id === runtimeFilterField.columnId)?.altName ?? '[undefined]',
            funcAltName: FunctionTypeNames[runtimeFilterField.func],
            value: String(runtimeFilterField.value).slice(0, 100) + (String(runtimeFilterField.value).length > 100 ? '..' : ''),
        });
    }
    const load = () => {
        setLoading(true);
        setError(false);
        axios(
            'http://main.demetric.172.38.128.2.nip.io/event-filter',
            {
                params: {}
            }
        ).then(function (response) {
            successResponse(response);
            setLoading(false);
        }).catch(() => {
            setError(true);
            setLoading(false);
        });
    }

    const activate = (active: boolean, onSuccess = () => {
    }) => {
        setSaving(true);
        setError(false);
        axios.post(
            'http://main.demetric.172.38.128.2.nip.io/event-filter/activate',
            {
                active
            }
        ).then(function (response) {
            successResponse(response);
            setSaving(false);
            onSuccess();
        }).catch(() => {
            setError(true);
            setSaving(false);
        });
    }

    const save = (data: DataTypeTree, onSuccess = () => {
    }) => {
        setSaving(true);
        setError(false);
        axios.post(
            'http://main.demetric.172.38.128.2.nip.io/event-filter',
            data
        ).then(function (response) {
            successResponse(response);
            setSaving(false);
            onSuccess();
        }).catch(() => {
            setError(true);
            setSaving(false);
        });


    }

    const clearRuntimeFilter = (onSuccess = () => {
    }) => {
        let newData = JSON.parse(JSON.stringify(data));
        remove(newData, (item: DataTypeFunc) => item.hasOwnProperty('runtime') && item.runtime === true);
        newData = foldAccordions(newData);
        save(newData, onSuccess);
    }

    const successResponse = (response: any) => {
        const resData = response.data?.body ?? [];
        const resActive = response.data?.active ?? false;
        dispatch(initData(resData));
        const newDataNormalize: dataNormalizeType = {};
        makeDataNormalize(resData, newDataNormalize);
        dispatch(initDataNormalize(newDataNormalize));
        dispatch(setActive(resActive));
        if (resActive) {
            dispatch(setCustomFilter(resData));
        } else {
            dispatch(setCustomFilter([]));
        }
    }

    return {fillAndActive: active && !!data.length, isActiveRuntimeFilter, runtimeFilterColumns, active, activate, data, loading, error, saving, save, load, clearRuntimeFilter}
}


