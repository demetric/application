import * as React from 'react';
import {useEffect} from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import Group from "./components/Group";
import {useDispatch, useSelector} from "react-redux";
import {dataNormalizeType} from "../../stores/filterSlice";
import {ConditionTypes} from "./types";
import {makeDataDenormalize} from "./tools";
import {Button} from "@mui/material";
import useFilter from "../../hooks/filter/useFilter";
import {LoadingButton} from "@mui/lab";


const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    right: '50%',
    height: '90%',
    display: 'block',
    transform: 'translate(-50%, -50%)',
    width: '90%',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};
const styleBody = {
    overflow: 'scroll',
    display: 'block',
    maxHeight: '92%',
};
export default function (props: self) {
    const {onClose, onSave} = props;
    const dispatch = useDispatch();
    const {data, load, save, active, activate, saving} = useFilter();

    const {dataNormalize}: {
        dataNormalize: dataNormalizeType,
    } = useSelector((state: any) => {
        return state.filter
    });

    const isElements = !!Object.values(dataNormalize).length;
    let isValid: boolean = isElements;

    for (const dataNormalizeKey in dataNormalize) {
        const {data} = dataNormalize[dataNormalizeKey];
        if (data.hasOwnProperty('columnId') && !data.columnId) {
            isValid = false;
        }
    }

    useEffect(() => {
        load();
    }, [dispatch]);

    return (
        <div>
            <Modal
                open={true}
                onClose={onClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Filter
                    </Typography>
                    <Box sx={styleBody}>
                        <Group uid={null} cond={ConditionTypes.And} data={data} hasParent={false}/>
                    </Box>
                    <Box sx={{display: 'flex', mt: 2}}>
                        <Box sx={{display: 'flex', gap: 2, marginLeft: 'auto'}}>
                            <Button size="large" onClick={onClose}>Cancel</Button>
                            {saving ?
                                <LoadingButton
                                    loading={true}
                                    variant="contained" size="large" color={'inherit'}>Saving</LoadingButton>
                                :
                                <>
                                    <Button variant="contained" size="large" color={active ? 'warning' : 'success'} disabled={!data.length} onClick={() => {
                                        activate(!active, () => onSave());
                                    }}>{active ? 'Deactivate' : 'Activate'}</Button>
                                    <Button variant="contained" size="large" color={'warning'} disabled={!isElements} onClick={() => {
                                        save([], () => onSave());
                                    }}>Clear</Button>
                                    <Button variant="contained" size="large" disabled={!isValid} onClick={() => {
                                        save(makeDataDenormalize(dataNormalize), () => onSave());
                                    }}>Apply</Button>
                                </>
                            }
                        </Box>
                    </Box>
                </Box>
            </Modal>
        </div>
    );
}

type self = {
    onClose: () => any,
    onSave: () => any,
}

