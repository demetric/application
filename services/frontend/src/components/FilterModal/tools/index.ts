import {dataNormalizeType} from "../../../stores/filterSlice";
import {DataTypeFunc, DataTypeGroup, DataTypeTree} from "../types";
import {isArray, remove} from "lodash";


export const makeDataNormalize = (items: DataTypeTree, dataNormalize: dataNormalizeType, parentUid: string | null = null): void => {
    for (const item of items) {
        const cond: string = item.cond;
        const uid: string | null = item.uid;
        const {active = false} = item;
        if (item.hasOwnProperty('func')) {
            const {value, columnId, func, runtime} = (item as DataTypeFunc);
            dataNormalize[uid as string] = {
                parentUid,
                data: {
                    active,
                    cond,
                    columnId,
                    func,
                    value,
                    runtime,
                }
            };
        } else {
            const {data} = (item as DataTypeGroup);
            if (uid) {
                dataNormalize[uid as string] = {
                    parentUid,
                    data: {
                        active,
                        cond,
                    }
                };
            }
            makeDataNormalize(data, dataNormalize, uid);
        }
    }
}

export const makeDataDenormalize = (dataNormalize: dataNormalizeType): DataTypeTree => {
    const dataNormalizeArray: Array<any> = [];
    for (const uid in dataNormalize) {
        const {data, parentUid} = dataNormalize[uid];
        dataNormalizeArray.push({
            uid,
            parentUid,
            ...data
        });
    }

    let tree: Array<any> = listToTree(dataNormalizeArray, {
        id: 'uid',
        parentId: 'parentUid',
        children: 'data',
    });

    deleteParentUid(tree);
    deleteEmptyGroups(tree);
    tree = foldAccordions(tree);

    return tree;
}


type ReType<T, K extends string> = T & { [P in K]?: ReType<T, K>[] }

export function listToTree<
    T extends object,
    C extends Pick<TreeOption<T>, 'id'> & {
        parentId: keyof T
        children: string & keyof R
    },
    R extends ReType<T, C['children']>,
>(list: T[], options: C): R[] {
    const res: R[] = []
    const fullMap = new Map<T[C['id']], T>(list.map((v) => [v[options.id], v]))

    for (const node of list) {
        const parent: R = fullMap.get(node[options.parentId]) as R
        if (parent) {
            if (!parent[options.children]) {
                parent[options.children] = [] as any
            }
            parent[options.children]!.push(node as any)
        } else {
            res.push(node as any)
        }
    }

    return res
}

interface TreeOption<T extends object> {

    id: keyof T
    children: keyof T
}


function deleteParentUid(tree: any) {
    for (const treeElement of tree) {
        if (treeElement.hasOwnProperty('data')) {
            deleteParentUid(treeElement.data);
        } else {
            delete treeElement.parentUid;
        }
    }
}


function deleteEmptyGroups(tree: any) {

    for (const treeElement of tree) {
        if (treeElement.hasOwnProperty('data')) {
            deleteEmptyGroups(treeElement.data);
        }
        removeEmptyGroupInGroup(treeElement.data);
    }
    removeEmptyGroupInGroup(tree);
}

const removeEmptyGroupInGroup = (tree: any) =>
    remove(tree, (item: DataTypeGroup) => !item.hasOwnProperty('func') && (!item.hasOwnProperty('data') || !isArray(item.data) || !item.data.length));

export function foldAccordions(tree: Array<any>): Array<any> {
    if (tree.length === 1 && tree[0].hasOwnProperty('data')) {
        return tree[0].data;
    }
    for (const treeElement of tree) {
        if (treeElement.hasOwnProperty('data')) {
            treeElement.data = foldAccordions(treeElement.data);
        }
    }
    return tree;
}