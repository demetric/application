import * as React from 'react';
import Box from '@mui/material/Box';
import {Button} from "@mui/material";
import AddIcon from '@mui/icons-material/Add';

export default function (props: self) {
    const {
        onClickFunc = () => {
        },
        onClickGroup = () => {
        }
    } = props;
    return (
        <Box sx={{display: 'flex', gap: 1, mt: 1}}>
            <Button startIcon={<AddIcon/>} variant="outlined" color={'secondary'} onClick={() => onClickFunc()}>Function</Button>
            <Button startIcon={<AddIcon/>} variant="outlined" onClick={() => onClickGroup()}>Group</Button>
        </Box>
    );
}


type self = {
    onClickFunc?: () => any,
    onClickGroup?: () => any,
}
