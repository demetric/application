import * as React from 'react';
import {ReactElement, useState} from 'react';
import Box from '@mui/material/Box';
import {Autocomplete, IconButton, Switch, TextField} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import Function, {FunctionType, FunctionTypes} from "../Function";
import AppendBox from "../AppendBox";
import {append, remove, update} from "../../../../stores/filterSlice";
import {useDispatch} from "react-redux";
import {omit, remove as deleteFromArray} from "lodash";
import {v4} from "uuid";
import {ConditionTypes, DataTypeFunc, DataTypeGroup, DataTypeTree} from "../../types";
import ArrowCircleLeftIcon from "@mui/icons-material/ArrowCircleLeft";
import ZoomOutMapIcon from '@mui/icons-material/ZoomOutMap';

const Group = (props: self): React.ReactElement<GroupType> => {
    const {
        onDelete = () => {
        },
        onMoveToBack = () => {
        },
        onDissolve = () => {
        },
        onMoveToBackFunc = () => {

        },
        data: dataProp = [],
    } = props;

    const [active, setActive] = useState(props.active ?? false);

    const [cond, setCond] = useState(props.cond);
    const [data, setData] = useState(dataProp);
    const pushChildren = (dataType: DataTypeGroup | DataTypeFunc) => {
        const newValue = [...data];
        newValue.push(dataType);
        setData(newValue);
    }

    const dispatch = useDispatch();

    const onUpdate = (mergeData: any) => {
        dispatch(update({
            data: {
                uid: props.uid,
                active,
                cond,
                ...mergeData
            }
        }));
    };

    const deleteChildren = (uid: string) => {
        const newValue = [...data];
        deleteFromArray(newValue, (item) => item.uid === uid);
        setData(newValue);
    }

    const printData = (items: DataTypeTree): Array<ReactElement> => {
        const components: Array<ReactElement<GroupType> | ReactElement<FunctionType>> = [];

        for (const item of items) {
            const cond: string = item.cond;
            const {active = false} = item;
            const uid: string | null = item.uid;
            if (item.hasOwnProperty('func')) {
                const {value, columnId, func, runtime} = (item as DataTypeFunc);
                components.push(
                    <Function
                        hasParent={!!props.uid}
                        key={uid}
                        runtime={runtime}
                        active={active}
                        uid={uid as string}
                        cond={cond as ConditionTypes}
                        func={func as FunctionTypes}
                        value={value}
                        columnId={columnId}
                        onDelete={deleteChildren}
                        onMoveToBack={(funcUid, dataFunc) => {
                            onMoveToBackFunc(dataFunc.uid, dataFunc);
                            deleteChildren(dataFunc.uid);
                            dispatch(remove(dataFunc.uid));
                        }}
                    />
                )
            } else {
                const {data} = (item as DataTypeGroup);
                components.push(
                    <Group
                        hasParent={!!props.uid}
                        key={uid}
                        active={active}
                        uid={uid}
                        cond={cond as ConditionTypes}
                        data={data}
                        onDelete={deleteChildren}
                        onMoveToBackFunc={(funcUid, dataFunc) => {
                            const data: DataTypeFunc = {...dataFunc, uid: v4()};
                            dispatch(append({parentUid: props.uid, data}));
                            pushChildren(data);
                        }}
                    />
                )
            }
        }
        return components;
    }


    return (
        <Box sx={{
            paddingInline: 1, pb: 1, border: '1px dashed grey', m: 2, ...(props.uid && !active ? {
                backgroundColor: 'lightgray',
            } : {})
        }}>
            <Box sx={{display: 'flex', gap: 1, pb: 3, pt: props.uid ? 2 : 0}}>{/*
                {(data.length && props.hasParent) ? <IconButton aria-label="rollback" color={'primary'}
                                                                   onClick={() => onMoveToBack(props.uid as string, {
                                                                       uid: props.uid as string,
                                                                       active,
                                                                       cond,
                                                                       data, // тут надо знать актуальное состояние дерева
                                                                   })}>
                    <ArrowCircleLeftIcon/>
                </IconButton> : ''}*/}
                {props.uid && <IconButton aria-label="delete" color={'warning'}
                                          onClick={() => {
                                              onDelete(props.uid as string);
                                              dispatch(remove(props.uid as string));
                                          }}>
                    <DeleteIcon fontSize="inherit"/>
                </IconButton>}
                {props.uid && <Switch checked={active} onClick={(e: any) => {
                    const active: boolean = !!e.target.checked;
                    setActive(active);
                    onUpdate({active: active});
                }}/>}
                {props.uid && <Autocomplete
                    size="small"
                    disabled={!props.uid}
                    disablePortal
                    disableClearable
                    options={Object.values(ConditionTypes)}
                    defaultValue={ConditionTypes.Or}
                    value={cond}
                    onChange={(i, option) => {
                        setCond(option as ConditionTypes)
                        onUpdate({cond: option});
                    }}
                    isOptionEqualToValue={(option, value) => option === value}
                    renderInput={(params) => <TextField sx={{
                        width: 180,

                    }} {...params} label="Group logic"/>}
                />}
                {/*     {(data.length && props.hasParent) ?
                    <IconButton aria-label="dissolve" color={'default'}
                                onClick={() => onDissolve(props.uid as string, data // тут так же надо знать актуальное стояние дерева)}>
                        <ZoomOutMapIcon/>
                    </IconButton>
                 : ''}*/}
            </Box>
            <Box>
                {printData(data)}
            </Box>
            <AppendBox
                onClickFunc={() => {
                    const data: DataTypeFunc = {uid: v4(), active: true, value: null, columnId: undefined, cond: ConditionTypes.And, func: FunctionTypes.contains};
                    dispatch(append({parentUid: props.uid, data}));
                    pushChildren(data);
                }}
                onClickGroup={() => {
                    const data: DataTypeGroup = {uid: v4(), active: true, cond: ConditionTypes.Or, data: []};
                    dispatch(append({parentUid: props.uid, data: omit(data, ['data'])}));
                    pushChildren({...data});
                }}
            />
        </Box>
    );
}

export default React.memo(Group);

export type GroupType = {}

export type self = {
    uid: string | null,
    hasParent: boolean,
    active?: boolean,
    cond: ConditionTypes,
    onDelete?: (uid: string) => any,
    onMoveToBack?: (uid: string, dataGroup: DataTypeGroup) => any,
    onDissolve?: (uid: string, data: DataTypeTree) => any,
    onMoveToBackFunc?: (uid: string, dataGroup: DataTypeFunc) => any,
    data?: DataTypeTree

}
