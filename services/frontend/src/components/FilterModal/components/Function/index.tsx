import * as React from 'react';
import {Autocomplete, IconButton, Switch, TextField} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import Box from "@mui/material/Box";
import {useDispatch} from "react-redux";
import {find} from 'lodash';
import {useState} from "react";
import {remove, update} from "../../../../stores/filterSlice";
import {ConditionTypes, DataTypeFunc} from "../../types";
import useColumn, {Column} from "../../../../hooks/event/useColumn";
import ArrowDropDownCircleIcon from '@mui/icons-material/ArrowDropDownCircle';
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft';

const Function = (props: self): React.ReactElement<FunctionType> => {

    const {
        onDelete = () => {
        },
        onMoveToBack = () => {
        },
    } = props;

    const [active, setActive] = useState(props.active ?? false);
    const [runtime, setRuntime] = useState(props.runtime ?? undefined);
    const [columnId, setColumnId] = useState(props.columnId);
    const [cond, setCond] = useState(props.cond);
    const [func, setFunc] = useState(props.func);
    const [value, setValue] = useState(props.value);
    const dispatch = useDispatch();
    const {columns} = useColumn();


    const selfData = (): DataTypeFunc => ({
        uid: props.uid,
        active,
        cond,
        columnId,
        func,
        ...runtime ? {runtime: true} : {},
        ...value ? {value} : {value: ''},
    });

    const onUpdate = (mergeData: any) => {
        dispatch(update({
            data: {
                ...selfData(),
                ...mergeData
            }
        }));
    }

    return (
        <Box sx={{
            display: 'flex', gap: 1, mb: 1, ...(!active ? {
                backgroundColor: 'lightgray',
            } : {})
        }}>
            {runtime && (
                <IconButton aria-label="constancy" color={'secondary'}
                            onClick={() => {
                                setRuntime(undefined);
                                onUpdate({runtime: false});
                            }}>
                    <ArrowDropDownCircleIcon/>
                </IconButton>
            )}
            {!runtime && props.hasParent && (<IconButton aria-label="rollback" color={'primary'}
                                                         onClick={() => onMoveToBack(props.uid, selfData())}>
                <ArrowCircleLeftIcon/>
            </IconButton>)}
            <IconButton aria-label="delete" color={'warning'}
                        onClick={() => {
                            onDelete(props.uid as string);
                            dispatch(remove(props.uid));
                        }}>
                <DeleteIcon/>
            </IconButton>
            <Switch disabled={runtime} checked={active} onClick={(e: any) => {
                const active: boolean = !!e.target.checked;
                setActive(active);
                onUpdate({active: active});
            }}/>
            <Autocomplete
                size="small"
                disabled={!props.uid || runtime}
                disablePortal
                disableClearable
                options={Object.values(ConditionTypes)}
                defaultValue={ConditionTypes.And}
                value={cond}
                onChange={(i, option) => {
                    setCond(option as ConditionTypes)
                    onUpdate({cond: option});
                }}
                isOptionEqualToValue={(option, value) => option === value}
                renderInput={(params) => <TextField sx={{
                    width: 180,

                }} {...params} label="Logic"/>}
            />
            <Autocomplete
                size="small"
                disabled={runtime}
                disablePortal
                disableClearable
                onChange={(i, option) => {
                    setColumnId(option.value);
                    onUpdate({columnId: option.value});
                }}
                options={columns.map(e => ({value: e.id, label: e.altName}))}
                renderInput={(params) => <TextField sx={{
                    width: 180,

                }} {...params} error={!columnId} label="Column"/>}
                isOptionEqualToValue={(option, value) => option.value === value.value}
                value={columnId ? {
                    value: columnId,
                    label: find(columns, (i: Column) => i.id === columnId)?.altName ?? '[undefined]'
                } : undefined}
            />
            <Autocomplete
                size="small"
                disabled={runtime}
                disablePortal
                disableClearable
                onChange={(i, option) => {
                    setFunc(option.value as FunctionTypes);
                    onUpdate({func: option.value});
                }}
                options={(Object.keys(FunctionTypes) as Array<keyof typeof FunctionTypeNames>)
                    .map((key) => (
                        {
                            value: key,
                            label: FunctionTypeNames[key] ?? key
                        }
                    ))}
                isOptionEqualToValue={(option, value) => option.value === value.value}
                value={{
                    value: FunctionTypes[func as keyof typeof FunctionTypes] as keyof typeof FunctionTypes,
                    label: FunctionTypeNames[func as keyof typeof FunctionTypeNames]
                }}
                defaultValue={{
                    value: FunctionTypes.contains,
                    label: FunctionTypeNames.contains
                }}
                renderInput={(params) => <TextField sx={{
                    width: 180,

                }} {...params} label="Function"/>}
            />
            <TextField disabled={runtime} size="small" sx={{
                width: '100%',
            }}
                       onChange={(i) => {
                           setValue(i.target.value);
                           onUpdate({value: i.target.value});
                       }}
                       value={value}
                       label="Value"/>
        </Box>
    );
}
export default React.memo(Function);

export type FunctionType = {}

export type self = {
    uid: string,
    hasParent: boolean,
    active?: boolean,
    columnId: string | number | undefined,
    cond: ConditionTypes,
    func: FunctionTypes,
    value: string | number | null | undefined,
    runtime?: true,
    onDelete?: (uid: string) => any,
    onMoveToBack?: (uid: string, dataFunc: DataTypeFunc) => any
}

// noinspection JSUnusedGlobalSymbols
export enum FunctionTypeNames {
    contains = 'like',
    containsNot = 'like not',

    startsWith = 'like start',

    startsWithNot = 'like start not',
    endsWith = 'like end',

    endsWithNot = 'like end not',
    eq = '=',
    ne = '<>',
    lt = '<',
    gt = '>',
    le = '<=',
    ge = '>=',
}

// noinspection JSUnusedGlobalSymbols
export enum FunctionTypes {
    contains = 'contains',
    containsNot = 'containsNot',

    startsWith = 'startsWith',

    startsWithNot = 'startsWithNot',
    endsWith = 'endsWith',

    endsWithNot = 'endsWithNot',
    eq = 'eq',
    ne = 'ne',
    lt = 'lt',
    gt = 'gt',
    le = 'le',
    ge = 'ge',
}