import {FunctionTypes} from "../components/Function";


export enum ConditionTypes {
    Or = 'or',
    And = 'and',
}

export type DataType = {
    active?: boolean,
    cond: ConditionTypes,
    uid: string,
}

export type DataTypeFunc = DataType & {
    func: FunctionTypes,
    columnId: string | number | undefined,
    value: string | number | null,
    runtime?: true,
}

export type DataTypeGroup = DataType & {
    data: Array<DataTypeFunc | DataTypeGroup>
}

export type DataTypeTree = Array<DataTypeFunc | DataTypeGroup>;