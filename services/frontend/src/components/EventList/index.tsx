import React, {useEffect, useState} from 'react'
import {useSelector} from 'react-redux'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import DeleteIcon from '@mui/icons-material/Delete';
import moment from "moment";
import {Box, Button, TablePagination} from "@mui/material";
import {useNavigate, useQuery} from "../../hooks/useNavigator";
import useList from "../../hooks/event/useList";
import {find} from "../../tools/arrayHelper";
import JsonViewer from "../JsonViewer";
import CellModal from "../CellModal";
import './styles.css';
import {remove, toInteger, uniq} from "lodash";
import {LoadingButton} from '@mui/lab';
import useEventTruncate from "../../hooks/event/useEventTruncate";
import useFilterModal from "../../hooks/filter/useFilterModal";
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import FilterModal from "../FilterModal";
import useFilter from "../../hooks/filter/useFilter";
import CellFilterButton from "../CellFilterButton";

const getDate = (date: any): string => moment(date).format('YYYY-MM-DD HH:mm:ss');

export function EventList() {
    const appList = useSelector((state: any) => {
        return state.application.list
    });

    const getAppName = (appId: any): string => (find(appList, 'id', appId, (i) => {
        return `[${i.project.name}] ${i.name}`
    }) ?? '[undefined]');

    const makeColumnNameModalCell = (row: any, columnName: string): string => {
        const createdAt = getDate(row.createdAt);
        const appName = getAppName(row.appId);
        return `[${row.id}] ${createdAt} ${appName}: ${columnName}`;
    };

    const list = useSelector((state: any) => {
        return state.event.list
    });
    const pagination = useSelector((state: any) => {
        return state.event.pagination
    });

    const indexList = useSelector((state: any) => {
        return state.eventIndexPath.list
    }).filter((i: any) => !i.isHidden);

    const {loadList} = useList();
    const {fillAndActive: isActiveFilter, isActiveRuntimeFilter, runtimeFilterColumns, load: loadFilter, clearRuntimeFilter} = useFilter();

    const query = useQuery();
    const page = query.get('page');
    const perPage = query.get('perPage') || pagination.perPage;

    const {navigate} = useNavigate();

    const [isHiddenBody, setHiddenBody] = useState(!!localStorage.getItem('isHiddenBody'));
    const hiddenBody = (hidden: boolean): void => {
        setHiddenBody(hidden);
        localStorage.setItem('isHiddenBody', hidden ? '1' : '');
    }

    const [cellModal, setCellModal] = useState<cellModalType | any>(null);

    type cellModalType = {
        columnName: string,
        body: any,
    };

    const getAimFromStorage = (): AimType => {
        const columnId = localStorage.getItem('aim[columnId]') ?? null;
        const rowId = localStorage.getItem('aim[rowId]') ?? null;
        const overName = localStorage.getItem('aim[overName]') ?? null;
        return {columnId, rowId, overName};
    }

    const [aim, setAimState] = useState<AimType>(getAimFromStorage());

    type AimType = {
        columnId?: string | null,
        rowId?: string | null,
        overName?: string | null,
    };

    const setAim = (position: AimType): void => {
        localStorage.setItem('aim[columnId]', position?.columnId ? String(position.columnId) : '');
        localStorage.setItem('aim[rowId]', position?.rowId ? String(position.rowId) : '');
        localStorage.setItem('aim[overName]', position?.overName ? String(position.overName) : '');
        setAimState(position);
    }

    const aimStyle = (position: AimType): any => {
        let count = 0;
        if (aim) {
            if (aim?.columnId && position?.columnId && String(aim?.columnId) === String(position?.columnId)) {
                count++;
            }
            if (aim?.rowId && position?.rowId && String(aim?.rowId) === String(position?.rowId)) {
                count++;
            }
        }
        if (count === 1) {
            return {backgroundColor: 'lightgrey'};
        }
        if (count > 1) {
            return {backgroundColor: 'cornsilk'};
        }
        return {};
    }

    const getDisallowModalFromStorage = (): string[] => {
        return (localStorage.getItem('disallowModal') ?? '').split(',');
    }

    const [disallowModal, setDisallowModalState] = useState<string[]>(getDisallowModalFromStorage());

    const hasDisallowModal = (id: string | number): boolean => {
        return disallowModal.includes(String(id));

    }

    const setDisallowModal = (id: string | number, disallow: boolean) => {
        let disallowModalUniq = [...disallowModal];
        if (disallow) {
            disallowModalUniq.push(String(id));
        } else {
            remove(disallowModalUniq, (i) => i === String(id));
        }
        uniq(disallowModalUniq);
        localStorage.setItem('disallowModal', disallowModalUniq.join(','));
        setDisallowModalState(disallowModalUniq);
    }


    const getCellValueHiddenFromStorage = (): string[] => {
        return (localStorage.getItem('cellValueHidden') ?? '').split(',');
    }

    const [cellValueHidden, setCellValueHiddenState] = useState<string[]>(getCellValueHiddenFromStorage());

    const hasCellValueHidden = (id: string | number): boolean => {
        return cellValueHidden.includes(String(id));

    }

    const setCellValueHidden = (id: string | number, disallow: boolean) => {
        let cellValueHiddenUniq = [...cellValueHidden];
        if (disallow) {
            cellValueHiddenUniq.push(String(id));
        } else {
            remove(cellValueHiddenUniq, (i) => i === String(id));
        }
        uniq(cellValueHiddenUniq);
        localStorage.setItem('cellValueHidden', cellValueHiddenUniq.join(','));
        setCellValueHiddenState(cellValueHiddenUniq);
    }

    const {loading: eventTruncateLoading, eventTruncate} = useEventTruncate({
        onSuccess: () => {
            setAim({columnId: null, rowId: null});
            navigate({page: 1});
            loadList();
        }
    });

    const {isShowModal: isShowFilterModal, showModal: showFilterModal} = useFilterModal();


    useEffect(() => {
        loadList();
        loadFilter();
    }, [page, perPage]);
    return (
        <>
            <Paper sx={{
                width: '100%', overflow: 'hidden',
            }}>
                <Box sx={{display: 'flex'}}>
                    {(aim && aim?.rowId && aim?.columnId) ?
                        (<Box sx={{display: 'flex', margin: 'auto', marginLeft: '15px', gap: 1}}>
                            <Button size={'small'} disabled={true} sx={{textTransform: 'none'}}
                                    variant="contained">{`[${aim.rowId}] ${aim.overName ?? aim.columnId}`}
                            </Button>
                            <Button size={'small'} variant="outlined"
                                    onClick={() => setAim({columnId: null, rowId: null})}>Clear aim</Button>
                        </Box>) : ''}


                    <Box sx={{display: 'flex', marginLeft: 'auto', gap: 1}}>
                        <Box sx={{display: 'flex', margin: 'auto'}}>
                            {isActiveRuntimeFilter && <Button
                                sx={{textTransform: 'none'}}
                                endIcon={<DeleteIcon/>}
                                onClick={() => clearRuntimeFilter(() => {
                                    if (toInteger(page ?? 1) !== 1) {
                                        navigate({page: 1});
                                    } else {
                                        loadList();
                                    }
                                })}
                                size={'small'} variant={'outlined'} color={isActiveFilter ? 'warning' : 'primary'}>RUNTIME FILTER{
                                runtimeFilterColumns.length === 1 ? ` ${runtimeFilterColumns[0].altName} ${runtimeFilterColumns[0].funcAltName} \`${runtimeFilterColumns[0].value}\`` : ''
                            }</Button>}
                        </Box>
                        <Box sx={{display: 'flex', margin: 'auto'}}>
                            <Button
                                endIcon={<FilterAltIcon/>}
                                onClick={() => showFilterModal()}
                                size={'small'} variant={isActiveFilter ? 'contained' : 'outlined'} color={isActiveFilter ? 'success' : 'info'}>Filter</Button>
                        </Box>
                        <Box sx={{display: 'flex', margin: 'auto'}}>
                            <LoadingButton
                                loading={eventTruncateLoading}
                                loadingPosition="end"
                                endIcon={<DeleteIcon/>}
                                onClick={() => eventTruncate()}
                                size={'small'} variant="contained" color="warning">Truncate all</LoadingButton>
                        </Box>
                        <TablePagination
                            component="div"
                            count={pagination.count}
                            page={pagination.page - 1}
                            onPageChange={(event, page) => {
                                page++;
                                navigate({page});
                            }
                            }
                            showFirstButton={true}
                            showLastButton={true}
                            rowsPerPage={pagination.perPage}
                            onRowsPerPageChange={(event) => navigate({perPage: event.target.value})}
                        />
                    </Box>
                </Box>
                <TableContainer sx={{maxHeight: 'calc(100vh - 105px)'}}>
                    <Table sx={{minWidth: 650}} aria-label="simple table" stickyHeader>
                        <TableHead>
                            <TableRow>
                                <TableCell key={'id'} width={30} style={aimStyle({columnId: 'id'})}>ID</TableCell>
                                <TableCell key={'createdAt'} width={280} style={aimStyle({columnId: 'createdAt'})}>Created at</TableCell>
                                <TableCell key={'appId'} width={280} style={aimStyle({columnId: 'appId'})}>App</TableCell>
                                {indexList.map((i: any) =>
                                    <TableCell key={'index' + i.id} className={'allow-modal-parent'} width={280}
                                               style={aimStyle({columnId: 'index' + i.id})}
                                               sx={hasCellValueHidden(i.id) ? {color: 'darkgoldenrod'} : undefined}
                                    >
                                        <div style={{cursor: 'pointer'}}
                                             onClick={() => setCellValueHidden(i.id, !hasCellValueHidden(i.id))}>{i.altName}</div>
                                        <Button className={'allow-modal'}
                                                variant={!hasDisallowModal(i.id) ? 'contained' : 'outlined'}
                                                size={'small'}
                                                onClick={() => setDisallowModal(i.id, !hasDisallowModal(i.id))}
                                                sx={{
                                                    bottom: '0',
                                                    minWidth: '4px',
                                                    padding: '4px',
                                                    lineHeight: '10px'
                                                }}>M</Button></TableCell>
                                )}
                                <TableCell key={'body'} style={{cursor: 'pointer', ...aimStyle({columnId: 'body'})}}
                                           onClick={() => hiddenBody(!isHiddenBody)}
                                           sx={isHiddenBody ? {color: 'darkgoldenrod'} : undefined}>Body</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {list.map((row: any) => (
                                <TableRow
                                    key={row.id}
                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                >
                                    <TableCell
                                        key={row.id + 'id'}
                                        onClick={() => setAim({
                                            columnId: 'id',
                                            rowId: row.id,
                                            overName: 'ID',
                                        })}
                                        style={{
                                            verticalAlign: 'top', ...aimStyle({
                                                columnId: 'id',
                                                rowId: row.id
                                            })
                                        }}>{row.id}</TableCell>
                                    <TableCell
                                        key={row.id + 'createdAt'}
                                        onClick={() => setAim({
                                            columnId: 'createdAt',
                                            rowId: row.id,
                                            overName: 'Created at',
                                        })}
                                        style={{
                                            verticalAlign: 'top', ...aimStyle({
                                                columnId: 'createdAt',
                                                rowId: row.id
                                            })
                                        }}>{getDate(row.createdAt)}</TableCell>
                                    <TableCell
                                        key={row.id + 'appId'}
                                        onClick={() => setAim({
                                            columnId: 'appId',
                                            rowId: row.id,
                                            overName: 'App',
                                        })}
                                        style={{
                                            verticalAlign: 'top', ...aimStyle({
                                                columnId: 'appId',
                                                rowId: row.id
                                            })
                                        }}>{getAppName(row.appId)}</TableCell>
                                    {indexList.map((col: any) =>
                                            find(row.eventIndexList, 'pathId', col.id, (i) => {
                                                return <TableCell key={row.id + 'index' + col.id} onDoubleClick={() => {
                                                    !hasDisallowModal(col.id) && setCellModal({
                                                        columnName: makeColumnNameModalCell(row, col.altName),
                                                        body: i.body
                                                    });
                                                }}
                                                                  onClick={() => setAim({
                                                                      columnId: 'index' + col.id,
                                                                      rowId: row.id,
                                                                      overName: col.altName,
                                                                  })}
                                                                  style={{
                                                                      verticalAlign: 'top', ...aimStyle({
                                                                          columnId: 'index' + col.id,
                                                                          rowId: row.id
                                                                      })
                                                                  }}>
                                                    <CellFilterButton columnId={col.id} value={i.body}/>
                                                    {!hasCellValueHidden(col.id) ? <JsonViewer body={i.body} wrap={true}
                                                                                               maxWidth={200}/> : '[hidden...]'}
                                                </TableCell>
                                            }) ?? <TableCell key={row.id + 'index' + col.id}
                                                             onClick={() => setAim({
                                                                 columnId: 'index' + col.id,
                                                                 rowId: row.id,
                                                                 overName: col.altName,
                                                             })}
                                                             style={{
                                                                 verticalAlign: 'top', ...aimStyle({
                                                                     columnId: 'index' + col.id,
                                                                     rowId: row.id
                                                                 })
                                                             }}>
                                                <CellFilterButton columnId={col.id} value={''}/>
                                            </TableCell>
                                    )}
                                    <TableCell key={row.id + 'body'} onDoubleClick={() => setCellModal({
                                        columnName: makeColumnNameModalCell(row, 'Body'),
                                        body: row.body
                                    })}
                                               onClick={() => setAim({
                                                   columnId: 'body',
                                                   rowId: row.id,
                                                   overName: 'Body',
                                               })}
                                               style={aimStyle({columnId: 'body', rowId: row.id})}>
                                        {!isHiddenBody ? <JsonViewer body={row.body}/> : '[hidden...]'}
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
            {
                cellModal && <CellModal
                    data={cellModal}
                    onClose={() => setCellModal(null)}
                />
            }
            {
                isShowFilterModal && <FilterModal onClose={() => showFilterModal(false)} onSave={() => {
                    showFilterModal(false);
                    loadList();
                }}/>
            }
        </>
    )
}
