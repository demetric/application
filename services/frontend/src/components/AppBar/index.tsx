import React, {useEffect} from 'react'
import {useSelector} from 'react-redux'
import {Switch} from "@mui/material";

import Alert from "@mui/material/Alert/Alert";
import {useNavigate, useQuery} from "../../hooks/useNavigator";
import {uniq, remove} from 'lodash';
import useList from "../../hooks/event/useList";
import {toArrNumberFromUrl} from "../../tools/queryParamHelper";

export function AppBar() {
    const appList = useSelector((state: any) => {
        return state.application.list
    });
    const {navigate} = useNavigate();
    const query = useQuery();
    const appId: number[] = toArrNumberFromUrl(query, 'appId');
    const {loadList} = useList();

    useEffect(() => {
        loadList();
    }, [query.get('appId')]);
    return (
        <div style={{display: 'flex', marginLeft: 'auto', gap: 5}}>
            {appList.map((i: any) => <Alert key={i.id} icon={false} variant={appId.includes(i.id) ? 'standard' : 'outlined'} severity="success">
                <Switch checked={appId.includes(i.id)} disabled={false}
                        color="success" size="small" onChange={(e) => {
                    e.target.checked ? appId.push(i.id) : remove(
                        appId, (el: any) => el === i.id);
                    navigate({appId: uniq(appId).join(','), page: 1});
                }}/>{`[${i.project.name}] ${i.name}`}</Alert>)}


        </div>
    )
}
