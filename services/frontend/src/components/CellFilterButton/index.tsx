import React from 'react'
import {Box, Button} from "@mui/material";
import {useNavigate, useQuery} from "../../hooks/useNavigator";
import useList from "../../hooks/event/useList";
import useFilter from "../../hooks/filter/useFilter";
import {remove, toInteger} from "lodash";
import {ConditionTypes, DataTypeFunc, DataTypeGroup} from "../FilterModal/types";
import {v4} from "uuid";
import {FunctionTypes} from "../FilterModal/components/Function";
import bodyToString from "../../tools/bodyToString";

export default React.memo(function (props: self) {
    const {navigate} = useNavigate();
    const {data, save, saving} = useFilter();
    const query = useQuery();
    const page: number = toInteger(query.get('page') ?? 1);
    const {loadList} = useList();

    const apply = () => {
        let newData = JSON.parse(JSON.stringify(data));
        remove(newData, (item: DataTypeFunc) => item.hasOwnProperty('runtime') && item.runtime === true);

        const runtimeFunc = {
            cond: ConditionTypes.And,
            active: true,
            uid: v4(),
            runtime: true,
            func: FunctionTypes.eq,
            columnId: props.columnId,
            value: bodyToString(props.value),
        } as DataTypeFunc;

        if (!newData.length) {
            newData.push(runtimeFunc);
        } else if (newData.length === 1 && !newData[0].hasOwnProperty('func')) {
            newData[0].cond = ConditionTypes.And;
            newData.push(runtimeFunc);
        } else {
            newData = [{
                cond: ConditionTypes.And,
                active: true,
                uid: v4(),
                data: newData,
            } as DataTypeGroup, runtimeFunc];
        }
        save(newData, () => {
            if (page !== 1) {
                navigate({page: 1});
            } else {
                loadList();
            }
        });
    }
    return (
        <Box sx={{position: 'relative'}}>
            <Box sx={{position: 'absolute', bottom: '-4px', left: '-16px'}}>
                <Button className={'allow-modal'}
                        variant={'outlined'}
                        disabled={saving}
                        size={'small'}
                        onClick={() => apply()}
                        sx={{
                            bottom: 0,
                            minWidth: '4px',
                            padding: '4px',
                            lineHeight: '10px',
                        }}>F</Button>
            </Box>
        </Box>
    )
})

type self = {
    columnId: string | number,
    value: any,
}