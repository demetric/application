import React from 'react'
import {Box, Modal, Typography} from "@mui/material";
import JsonViewer from "../JsonViewer";

const style = {
    bgcolor: 'background.paper',
    border: '2px solid #000',
    position: 'absolute',
    top: '1%',
    left: '1%',
    right: '1%',
    height: '97%',
    display: 'block'
};
const styleBody = {
    overflow: 'scroll',
    display: 'block',
    height: '97%',
};
export default function (props: propType) {
    const {data, data: {columnName, body}, onClose} = props;
    const {} = props;

    return (
        <Modal
            keepMounted
            open={!!data}
            onClose={onClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <Typography id="modal-modal-title" variant="h5" component="h2">
                    {columnName}
                </Typography>
                <Box sx={styleBody}>
                    <JsonViewer body={body} />
                </Box>
            </Box>
        </Modal>
    )
}

type propType = {
    data: {
        columnName: string,
        body: any,
    },
    onClose: () => any,
}