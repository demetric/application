import React from 'react'
import Alert from "@mui/material/Alert/Alert";
import JSONPretty from "react-json-pretty";
import "./styles.css";

export default function (props: propType) {
    const {body, wrap, maxWidth} = props;
    const {} = props;

    const className = wrap ? 'json-wrap' : undefined;

    const style: any = {};

    if (maxWidth) {
        style['maxWidth'] = maxWidth + 'px';
    }

    return (
       typeof body === 'object' ? (
            <>{body.hasOwnProperty('__error__') && (
                <Alert severity="error">{body.__error__}</Alert>)}
                {body.hasOwnProperty('__var_dump__') ?
                    (<div dangerouslySetInnerHTML={{__html: body.__var_dump__}}/>)
                    : (<JSONPretty className={className} style={style} data={body}></JSONPretty>)}</>
        ) : (
            <div className={className} style={style}>{body}</div>)
    )
}

type propType = {
    body: any,
    wrap?: boolean,
    maxWidth?: number,
}