import React, {useState, useEffect} from 'react';
import io from 'socket.io-client';
import {EventList} from "./components/EventList";
import {setStreamActive, unshiftOnline} from './stores/eventSlice';
import {useDispatch, useSelector} from "react-redux";
import Alert from '@mui/material/Alert/Alert';
import {Switch} from "@mui/material";
import useList from "./hooks/event/useList";
import {AppBar} from "./components/AppBar";

const SERVER_SOCKET_URL = 'http://frontend-trigger.172.38.128.2.nip.io';
const socket = io(SERVER_SOCKET_URL);

function App() {
    const [isConnected, setIsConnected] = useState(socket.connected);

    const activeState = useSelector((state: any) => {
        return {
            page: state.event.pagination.page,
            isLoaded: state.event.isLoaded,
            streamActive: state.event.streamActive,
        };
    });

    const active = activeState.page < 2 && activeState.isLoaded && activeState.streamActive;
    const systemActive = activeState.page < 2 && activeState.isLoaded;

    const dispatch = useDispatch();
    const {loadList} = useList();
    useEffect(() => {
        socket.on('connect', () => {
            setIsConnected(true);
            if (active) {
                loadList();
            }
        });

        socket.on('disconnect', () => {
            setIsConnected(false);
        });

        socket.on('pong', (args) => {
            dispatch(unshiftOnline(args));
        });
        return () => {
            socket.off('connect');
            socket.off('disconnect');
            socket.off('pong');
        }
    }, []);

    return (
        <div>
            <div style={{display: 'flex'}}>
                <div style={{display: 'flex'}}>
                    <Alert severity={isConnected ? 'success' : 'error'}>{isConnected ? 'Socket connected' : 'Socket disconnected'}</Alert>
                    <Alert severity={active ? 'info' : 'warning'}><Switch checked={!!active} disabled={!systemActive} size="small" onChange={(e) => {
                        dispatch(setStreamActive(e.target.checked));
                        if (e.target.checked) {
                            loadList();
                        }
                    }}/> {active ? 'Stream activated' : 'Stream deactivated'}</Alert>
                </div>
                <AppBar/>
            </div>

            <EventList/>
        </div>
    );
}

export default App;